# EMTF Docs

This is the documentation for the documentation.

## Setup
```bash
pip install -r requirements.txt
npm install katex # Needed to render math
```

## Testing page locally

```bash
cd $project_folder
mkdocs serve
```

The page will be served in [http://127.0.0.1:8000](http://127.0.0.1:8000/) and will be updated anytime any of the files in the directory is updated, this is useful for checking our changes before pushing.

## Building Docker Image

This is not necessary as gitlab will automatically do this for us, but in case there's a bug we can do it ourselves.

```bash
cd $project_folder
docker build -t emtf-docs .
```

## Compiling webpage

This is not necessary as gitlab will automatically do this for us, but in case there's a bug we can do it ourselves.

```bash
cd $project_folder
mkdocs build
```

This creates a folder called "site" which contains all the static html files.
