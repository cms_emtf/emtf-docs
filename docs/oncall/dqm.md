# DQM Monitoring

- Author: Osvaldo Miguel Colin
- Last Updated: 01/June/2023

## Responsabilities
- Monitor the main page L1TStage2EMTF (these are the main plots).
- Monitor the plots in the subdirectories such as the Timing and CSC specific plots.
- If you see any issues notify the relevant people! Can be a problem in EMTF or in the other 
muon subsystems

