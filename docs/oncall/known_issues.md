# Known Issues

- Author: Osvaldo Miguel Colin
- Last Updated: 10/July/2023

## CSC Chambers - Open

<table>
  <tr>
    <th>From (Run)</th>
    <th>To (Run)</th>
    <th>Link (SP - Link)</th>
    <th>Chamber</th>
    <th>Swatch Observation</th>
    <th>DQM Observation</th>
    <th>Resolution</th>
  </tr>
  <tr>
    <td>May 15, 2023 (Run=367516)</td>
    <td></td>
    <td>EMTFm6 - me1b_02</td>
    <td>ME-1/1a 1 and ME-1/1b 1</td>
    <td style="color: red">Error: isAligned = False and isLocked = False</td>
    <td style="color: red">Missing in CSC Occupancy plots in DQM</td>
    <td>Leave as is for now.</td>
  </tr>
  <tr>
    <td>Not Tracked</td>
    <td></td>
    <td>EMTFm1 - me4_5</td>
    <td>ME-4/2 4</td>
    <td style="color: green">OK</td>
    <td style="color: red">Missing in CSC Occupancy plots in DQM</td>
    <td>Leave as is for now.</td>
  </tr>
</table>

## CSC Chambers - Resolved

<table>
  <tr>
    <th>From (Run)</th>
    <th>To (Run)</th>
    <th>Link (SP - Link)</th>
    <th>Chamber</th>
    <th>Swatch Observation</th>
    <th>DQM Observation</th>
    <th>Resolution</th>
  </tr>
  <tr>
    <td>May 24, 2023 (Run=367910)</td>
    <td>July 7, 2023</td>
    <td>EMTFm4 - me1a_03</td>
    <td>ME-1/1a 23 and ME-1/1b 23</td>
    <td style="color: green">OK</td>
    <td style="color: red">Missing in CSC Occupancy plots in DQM</td>
    <td>Fixed by CSC after Marina reported it.</td>
  </tr>
</table>
