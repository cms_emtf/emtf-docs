# General Information

- Author: Osvaldo Miguel Colin
- Last Updated: 01/June/2023

## Contacts
- EMTF DOC: +165404 (+41 75 411 5404)
- CSC DOC: +161972
- L1 DOC: 
    - Phone Number: +161958
    - Email: cms-l1t-technical-coordination@cern.ch
    - Current: Varun Sharma & Karol Bunkowski

## Mattermost Channels
- CMS Online Ops/Run 3: Shifters + DOCs

## Meetings
- Daily Run Meeting:
    - [Indico Page](https://indico.cern.ch/event/1009415/)
    - Time: 9:30 hrs
    - `Note: Usually only attend daily run meetings when EMTF has something to report`

## Twiki Pages
- [L1 EMTF Expert Page](https://twiki.cern.ch/twiki/bin/viewauth/CMS/EMTFOnCallExpert)
    - `Note: needs updating!`
- [EMTF SWATCH Control Software](https://twiki.cern.ch/twiki/bin/viewauth/CMS/EMTFSwatchControlSoftware)
- [EMTF Data Quality Monitoring](https://twiki.cern.ch/twiki/bin/viewauth/CMS/EMTFDataQualityMonitoring)
    - For plot descriptions and references
- [Shift List](https://cmsonline.cern.ch/webcenter/portal/cmsonline/Common/Shiftlist)

## Elogs
- Under: Home>Subsystems>Trigger>MuTF

## Useful links
- [EMTF Tutorial - 14/Jul/2022](https://indico.cern.ch/event/1180620/#b-469897-emtf-tutorial-emtf-tu)
