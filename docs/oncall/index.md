# Getting Started

- Author: Osvaldo Miguel Colin
- Last Updated: 01/June/2023

## Requirements
- **Required grid certificate and cmsusr account** (P5 network account)
    - Can request a cmsusr account by making a JIRA ticket
- Access to USC and P5 control room
    - **Need trainings and access requests**

## Responsabilities
- **EMTF Detector On Call Expert (DOC)** is responsible for monitoring the EMTF 
system, identifying/solving issues that arise, and notifying other experts in 
EMTF
- Holder of **EMTF phone** (we’ve been using CERN Phone App)
- EMTF DOC **communicates with shifters** at P5, usually CSC and L1
- Attend and update in **EMTF and CSC Meetings**


## Useful links
- [EMTF Tutorial - 14/Jul/2022](https://indico.cern.ch/event/1180620/#b-469897-emtf-tutorial-emtf-tu)
