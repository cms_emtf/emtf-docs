# Week 22 - 2023

- On Call: Efe Yigitbasi
- Shadow: Osvaldo Miguel Colin

## Missing Chamber from May 15 to June 1st


**CSC Chamber Occupancy Summary**

<table>
  <tr>
    <th>Date</th>
    <th>Fill</th>
    <th>Runs</th>
    <th>ME-1/1a 1</th>
    <th>ME-1/1b 1</th>
    <th>ME-1/1a 23</th>
    <th>ME-1/1b 23</th>
    <th>ME-4/2 4</th>
  </tr>
  <tr>
    <td>May 30</td>
    <td>8853</td>
    <td>368229</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 30</td>
    <td>8850</td>
    <td>368223</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 24</td>
    <td>8822</td>
    <td>367910</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 23</td>
    <td>8821</td>
    <td>367883</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 23</td>
    <td>8817</td>
    <td>367838</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 22</td>
    <td>8816</td>
    <td>367831</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 22</td>
    <td>8811</td>
    <td>367790</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 21</td>
    <td>8807</td>
    <td>367770</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 21</td>
    <td>8804</td>
    <td>367758</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 20</td>
    <td>8796</td>
    <td>367742</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 19</td>
    <td>8794</td>
    <td>367730</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 18</td>
    <td>8786</td>
    <td>367696</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 17</td>
    <td>8784</td>
    <td>367665</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 16</td>
    <td>8782</td>
    <td>367615</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 15</td>
    <td>8775</td>
    <td>367553</td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:red"><b>MISSING</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
  <tr>
    <td>May 15</td>
    <td>8773</td>
    <td>367516</td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:green"><b>NORMAL</b></td>
    <td style="color:red"><b>MISSING</b></td>
  </tr>
</table>
