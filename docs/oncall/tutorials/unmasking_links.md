# Unmasking Links

- Author: Osvaldo Miguel Colin
- Last Updated: 05/July/2023

## Walkthrough

- Go to [L1CE Webpage](https://l1ce.cms/editor/EMTF/RS/)
- Navigate to EMTF Run Settings
    - Select Subsystem -> EMTF
    - Select Section -> Run Settings
- Select the relevant key, by clicking on the name.
    - This will display the key info panel.
    - You can click on the key info panel where the key name is displayed to show more information about the current key.
- On the key info panel there's a CONF section. Click on the pencil, it will display the key's contents.
    - At this point you should be able to see the key's contents.
    - If you aren't in WRITE mode yet, you can switch by clicking on the mode->READ label in the top right corner of the page.
- The keys are written in XML, you can comment or uncomment a link as needed following the syntax used in XML.
- Once you've done the necessary changes, you can queue your changes by clicking on the queue button in the top left corner of the key info panel.
    - You will be prompted to provide a comment on the changes.
    - Please choose something meaningful, so that others know what you did.
- Click on the pending changes button on the top right corner of the page.
    - This will redirect you to the review page.
    - Make sure all the changes you did are there and that they are the expected changes.
- After verifying your changes, click on the commit button.
- Write down the new keys and share them with the L1 Doc and EMTF Docs.

## Visual Walkthrough

Unmasking a chamber in two different keys.

![](assets/unmasking_links.gif)

