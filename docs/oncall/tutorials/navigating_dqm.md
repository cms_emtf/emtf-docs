# Navigating DQM

- Author: Osvaldo Miguel Colin
- Last Updated: 05/July/2023

## Walkthrough
- Go to [DQM Website](https://cmsweb.cern.ch/dqm/online)
- Navigate to EMTF Plots
    - Go to L1T -> L1TStage2EMTF
- Select a valid run
    - Click on the current Runnumber.
    - You will be redirected to a page where you can search runnumbers.
      - Write a runnumber in the searchbar, the results will show up below.
      - Select a runnumber from the search results; you will be redirected back to the plot section.
- At this point you should be able to see valid DQM plots.
    - You can double click on any of these plots to open them.

## Visual Walkthrough
![](assets/moving_in_dqm.gif)

