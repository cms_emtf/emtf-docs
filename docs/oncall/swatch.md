# Swatch Monitoring

- Author: Osvaldo Miguel Colin
- Last Updated: 05/July/2023

## Responsabilities
- Monitor the EMTF Swatch Cell and make sure the aren't any links in error state.
- If there are, verify with DQM that these chambers are indeed missing and report them to the relevant subsystem.

## CSC Link Mapping

Link label: Used in Swatch
Chamber Name Format: Station/Ring/Chamber.

![](assets/link_mapping.png)

**How to Read**

- Find the cell with the Station/Ring/Chamber you're interested in.
- Look up the column where that cell is, the column indicates the sector processor.
    - The sector processor can be positive or negative, depending on whether the chamber is on the negative or positive endcap.
    - Note that chambers are reported in Swatch as EMTFpN or EMTFmN where N is the Sector Procesor Number going from 1 to 6.
- Look up the row where that cell is, the row indicates the link name.
- Example Chamber -1/1/1:
    - Station=-1 Sector=1 Chamber=1
    - Column: Sector+/-6.
        - We pick the negative Sector Procesor since the station is negative.
        - Therefore EMTFm6.
    - Row: me1b_2.
    - Put it all together and you get EMTFm6 - me1b_2.

