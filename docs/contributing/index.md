# Contributing

## Early Adopters

- Workflow: [Web](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow) Feature Branch

1. Ask a maintainer to add you to the project and create a feature branch for you.
2. Checkout your feature branch and start writting docs.
3. Once you're done, make a merge request (a.k.a. pull request in github)
4. Get merged to master and see your changes on the production site.

## Maintainers

- Osvaldo Miguel Colin
    - Email: osvaldo.miguel.colin@cern.ch

## Links

- [Repository](https://gitlab.cern.ch/cms_emtf/emtf-docs)

