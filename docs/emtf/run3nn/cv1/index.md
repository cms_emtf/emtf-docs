# Run3 NN Converted Inputs

- Author: Osvaldo Miguel Colin
- Last Updated: 17/March/2023

## Remarks

- Tracks used in these studies are mode 11, 13, 14, and 15 only.

## Variables

![](assets/dataset.png)

- x_dphi
- x_dtheta
- x_bend_emtf
- x_fr_emtf
- x_track_theta
- x_ME11ring
- x_RPCbit
- x_mode
- x_straightness

