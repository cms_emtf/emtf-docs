# ucv4

- Author: Osvaldo Miguel Colin
- Last Updated: 17/March/2023

Neural Network Training

After various trainings, I've empirically come to the conclusion that training the NN to target both $`p_{T}`$ and $`d_{xy}`$, 
makes it harder to minimize the error on both parameters. I've found that similar performance can be achieved by splitting the
dense 20-15 node hidden layer model in two 10-8 (20-16 node when combined) node hidden layer models.

Training two sub-models and then stitching them together requires both models to share a common layer. 
The common layer can be either at the beginning or at the end of the submodels. 
In this case since both NN use the same inputs I've decided to stitch them after the first batch normalization.

The training is performed one at a time. First I train the $`p_{T}`$ sub-model and then the $`d_{xy}`$ sub-model.
When I train the $`d_{xy}`$ sub-model I re-use the weights from the $`p_{T}`$ sub-model training for the first Batch Normalization, 
ensuring both sub-models have identical Batch Normalization.

Once both trainings have been completed I stitch the two neural networks, by copying over the weights from each training to an empty 20-16 node model.
It should be noted that even-though this model is larger by one node in the second hidden layer, it has less non-zero weights! More on this later.
After all the weights have been copied, the combined NN unsurprisingly predicts both parameters, with the same performance of two individual NN.

I would like to take a moment to point out that stitching isn't actually necessary. If we had enough resources in the FPGA to run multiple NN simultaneously,
this may actually be a better option. However, since I'm trying to reduce the amount of resources used, I've decided that in this way I can zero-out a couple of weights.

Advantages of Stitching

- Uncouples the regressing for $`p_{T}`$ and $`d_{xy}`$.
  - This opens the possibility of using different loss functions, depending on the parameter targeted during training!
  - Since the training is done separately, minimization of one parameter doesn't affect the other!
  - In theory we could use different activation functions for each parameter.
    - This is actually being tested for the next version: ucv5.
- Has less non-zero weights.
  - Using our current design, we have the Input Layer-BatchNormalizaiton-2x(Hidden-Layer-BatchNormalization)-Activation Function.
  - Each Batch Normalization introduces 4 weights. 
  - Each Dense Layer introduces MxN+N weights (+N comes from bias, if no bias is used, it's just MxN).
  - The total number of weights of a 20-16 node model would have is: 
    - $`\textrm{N Weights} = 29*4 + 20*(29+4) + 16*(20+4) + 16*2 + 2=1194`$
  - However since the model is only densely connected up to the first Batch Normalization, the stitched version has:
    - $`\textrm{N Non-Zero Weights} = 29*4 + 20*(29+4) + 2 * \left [ 8*(10+4) + 8*1+1 \right ] =1018`$
    - Therefore 176 weights are zero.

Why I moved away from weighing training sample to increase $`p_{T}`$ efficiency?

- It turns out that by doing this, the NN learns to over-estimate $`p_{T}`$ for low $`p_{T}`$ muons,
  this would result in a large rate increase, which I don't think is worth it.


Internal Layers

- Dense 20 Node Layer
- Dense 16 Node Layer

Variables (Input)

- Used white noise for invalid dphi
- Used white noise for invalid dtheta

Predictions

<table>
  <tr>
    <th>Type</th>
    <th>$`p_{T}`$</th>
    <th>$`d_{xy}`$</th>
  </tr>
  <tr>
    <td>True</td>
    <td><img src="../assets/ucv4/pt_true.png"/></td>
    <td><img src="../assets/ucv4/dxy_true.png"/></td>
  </tr>
  <tr>
    <td>Predicted</td>
    <td><img src="../assets/ucv4/pt_pred.png"/></td>
    <td><img src="../assets/ucv4/dxy_pred.png"/></td>
  </tr>
</table>

RMSE

<table>
  <tr>
    <th>Type</th>
    <th>$`1/p_{T}`$</th>
    <th>$`p_{T}`$</th>
    <th>$`p_{T}`$</th>
    <th>$`d_{xy}`$</th>
  </tr>
  <tr>
    <td>Inclusive</td>
    <td><img src="../assets/ucv4/pt_0_to_inf.png"/></td>
    <td><img src="../assets/ucv4/pt_rel_0_to_inf.png"/></td>
    <td><img src="../assets/ucv4/invpt_0_to_inf.png"/></td>
    <td><img src="../assets/ucv4/dxy_0_to_inf.png"/></td>
  </tr>
  <tr>
    <td>$`0 - 10\ \text{GeV}`$</td>
    <td><img src="../assets/ucv4/pt_0_to_10.png"/></td>
    <td><img src="../assets/ucv4/pt_rel_0_to_10.png"/></td>
    <td><img src="../assets/ucv4/invpt_0_to_10.png"/></td>
    <td><img src="../assets/ucv4/dxy_0_to_10.png"/></td>
  </tr>
  <tr>
    <td>$`10 - 20\ \text{GeV}`$</td>
    <td><img src="../assets/ucv4/pt_10_to_20.png"/></td>
    <td><img src="../assets/ucv4/pt_rel_10_to_20.png"/></td>
    <td><img src="../assets/ucv4/invpt_10_to_20.png"/></td>
    <td><img src="../assets/ucv4/dxy_10_to_20.png"/></td>
  </tr>
  <tr>
    <td>$`20 - \inf\ \text{GeV}`$</td>
    <td><img src="../assets/ucv4/pt_20_to_inf.png"/></td>
    <td><img src="../assets/ucv4/pt_rel_20_to_inf.png"/></td>
    <td><img src="../assets/ucv4/invpt_20_to_inf.png"/></td>
    <td><img src="../assets/ucv4/dxy_20_to_inf.png"/></td>
  </tr>
</table>

Correlations

<table>
  <tr>
    <th>Type</th>
    <th>$`p_{T}`$</th>
    <th>$`d_{xy}`$</th>
  </tr>
  <tr>
    <td>Prediction</td>
    <td><img src="../assets/ucv4/vld_pt.png"/></td>
    <td><img src="../assets/ucv4/vld_dxy.png"/></td>
  </tr>
  <tr>
    <td>Error</td>
    <td><img src="../assets/ucv4/vld_pt_err.png"/></td>
    <td><img src="../assets/ucv4/vld_dxy_err.png"/></td>
  </tr>
</table>

Efficiencies

<table>
  <tr>
    <th>Variable</th>
    <th>$`p_{T}`$</th>
    <th>$`d_{xy}`$</th>
    <th>$`\eta^{*}`$</th>
  </tr>
  <tr>
    <td>$`p_{T}`$</td>
    <td><img src="../assets/ucv4/vld_eff_vs_pt.png"/></td>
    <td><img src="../assets/ucv4/vld_eff_vs_dxy.png"/></td>
    <td><img src="../assets/ucv4/vld_eff_vs_eta_star.png"/></td>
  </tr>
</table>

