# Run 3 NN Unconverted Inputs v4

- Author: Osvaldo Miguel Colin
- Last Updated: 17/March/2023

## Remarks

- Tracks used in these studies are mode 11, 13, 14, and 15 only.

## Variables

![](assets/dataset.png)

- x_dphi
- x_dphi_sign
- x_dtheta
- x_dtheta_sign
- x_pattern_csc
- x_track_theta
- x_track_mode

## Dataset Cross-check w/ Converted Inputs [Red]

![](assets/dataset_check.png)

- Row 1: Signed dphi
- Row 2: Signed dtheta
- Row 3: $`p_T`$ and $`d_{\text{xy}}`$

