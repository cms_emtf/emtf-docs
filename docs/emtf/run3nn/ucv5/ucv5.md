# ucv5

- Author: Mason Weiss
- Last Updated: 23/March/2023

### Goal
* Aim to determine various improvements to the split-model structure from ucv4 without restrictions on resource use
* Improve $`p_{t}`$ prediction using $`q \cdot p_{t}`$ prediction instead of regular $`p_{t}`$
     * This is described in more depth below in items 1 & 2  
* Improve $`d_{xy}`$ prediction using a variety of means as listed below:     
     * **tanh**($`d_{xy}`$) inner activation layer | **relu**($`d_{xy}`$) output activation layer **CONTROL**
     * **tanh**($`d_{xy}`$) inner activation layer | **trainable softplus**($`d_{xy}`$) output activation layer
     * **relu**($`d_{xy}`$) inner activation layer | **relu**($`d_{xy}`$) output activation layer
     * **relu**($`d_{xy}`$) inner activation layer | **trainable softplus**($`d_{xy}`$) output activation layer

### Summary of Changes
1. Implemented $`q \cdot p_{t}`$ training:
     * **tanh** inner activation layer
     * Hybrid output activation layers
           * **tanh** for $`q \cdot p_{t}`$ output activation layer
           * **relu** for $`d_{xy}`$ output activation layer
           * These are stitched together seamlessly in the final model so that raw outputs from the last dense layer of the $`q \cdot p_{t}`$ portion recieve **tanh** activation, and the other half recieves **relu** activation.  
     * Retained the same structure from ucv4, so two loss functions required
           * train $`p_{t}`$ component of NN by calculating loss on $`q/p_{t}`$; loss function remaind as logcosh
              * entire range of $`q \cdot p_{t}`$ then can be generated through the output activation layer of **coth**
           * use logcosh for $`d_{xy}`$
&nbsp;
2. Accordingly, a new training analyzer is defined in *qpt_and_dxy_training.py*, which utilizes a symmetric range in each plot, so that $`q \cdot p_{t}`$ plots can be created
     * All validation plots have been updated to reflect this
     * efficiency and $`\eta*`$ plots remain with $`p_{t}`$ , just taken as the absolute value of $`q \cdot p_{t}`$ output
     * One $`p_{t}`$ validation plot remains to display the "fanning out" behavior of the model's predictions  
&nbsp;   
3. In order to better test on just the $`d_{xy}`$ half of the neural network, a third training analyzer is defined in *dxy_training.py*, which solely analyzes the $`d_{xy}`$ predictions and is compatible only with the $`d_{xy}`$ half
     * Within this training analyzer, new bins are defined for the $`d_{xy}`$ error
     * Output from this error generator is of the form:
           * (0,inf), (0,10), (10,20), (20,32), (32,inf)
           * where each bin is associated with the true $`d_{xy}`$ value in cm
           * accordingly, plots are generated for true and predicted $`d_{xy}`$ in each of these ranges
     * Flags were added to the parser in *command.py* so that these training analyzers can be toggled on/off  
&nbsp;     
4. Introduced a custom activation layer, **softplus**, which has two trainable parameters
     * Softplus is defined as:
           $`softplus(x,N) = 1/N * (1 + exp(N*x)`$
     * As N approaches infinity, **softplus** closely matches **relu**
     * For this activation layer, I set $`N`$ and an offset parameter, $`\beta`$, to be trainable
     * This way, the neural network *should* determine an ideal value for both of these parameters  
&nbsp;
5. Experiment with the various permutations of activation layers described above, and publish findings below  
&nbsp;
6. Preliminary trainings were performed using the following characteristics:
     * max learning rate = 0.0063
     * epochs = 150
     * batch size = 4096
&nbsp;
7. Perform secondary trainings with select characteristics from the preliminary results
     * max learning rate = 0.0063
     * epochs = 150
     * batch size = 4096
&nbsp;
8. Stitch the model together, which includes creating a hybrid inner activation function and calling both training analyzers
     * Compare the results from both $`d_{xy}`$ and $`p_{t}`$ in ucv4 and the selected models from ucv5
           * max learning rate = 0.0063
           * epochs = 300
           * batch size = 2048
      * Consider retaining the best activation function for $`d_{xy}`$ for use in $`p_{t}`$, and analyze the effects of this proposed change
&nbsp;
### Preliminary Testing (for dxy only)

#### True Distribution
<table>
  <tr>
    <th>Inclusive</th>
  </tr>
  <tr>
    <td><img src="../assets/0/dxy_true.png"/></td>
</table>
&nbsp;

#### Predicted Distribution and Error
<table>
  <tr>
    <th>Inner $`d_{xy}`$ Activation Layer</th>
    <th>Output $`d_{xy}`$ Activation Layer</th>
    <th>$`d_{xy}`$ Predicted</th>
    <th>$`d_{xy}`$ Inclusive Error</th>
  </tr>
  <tr>
    <td><b>tanh</b></td>
    <td><b>relu</b></td>
    <td><img src="../assets/0/dxy_pred.png"/></td>
    <td><img src="../assets/0/dxy_0_to_inf.png"/></td>
  </tr>
    <tr>
    <td><b>tanh</b></td>
    <td><b>trainable softplus N = 0.022325  B = -3.406175 </b></td>
    <td><img src="../assets/1/dxy_pred.png"/></td>
    <td><img src="../assets/1/dxy_0_to_inf.png"/></td>
  </tr>
    </tr>
    <tr>
    <td><b>relu</b></td>
    <td><b>relu</b></td>
    <td><img src="../assets/2/dxy_pred.png"/></td>
    <td><img src="../assets/2/dxy_0_to_inf.png"/></td>
  </tr>
    </tr>
    <tr>
    <td><b>relu</b></td>
    <td><b>trainable softplus N = 0.003811 B = -4.898549 </b></td>
    <td><img src="../assets/3/dxy_pred.png"/></td>
    <td><img src="../assets/3/dxy_0_to_inf.png"/></td>
  </tr>
</table>
&nbsp;

#### Validation and Error
<table>
  <tr>
    <th>Inner $`d_{xy}`$ Activation Layer</th>
    <th>Output $`d_{xy}`$ Activation Layer</th>
    <th>$`d_{xy}`$ Correlation</th>
    <th>$`d_{xy}`$ Error</th>
  </tr>
  <tr>
    <td><b>tanh</b></td>
    <td><b>relu</b></td>
    <td><img src="../assets/0/vld_dxy.png"/></td>
    <td><img src="../assets/0/vld_dxy_err.png"/></td>
  </tr>
    <tr>
    <td><b>tanh</b></td>
    <td><b>trainable softplus N = 0.022325  B = -3.406175 </b></td>
    <td><img src="../assets/1/vld_dxy.png"/></td>
    <td><img src="../assets/1/vld_dxy_err.png"/></td>
  </tr>
    </tr>
    <tr>
    <td><b>relu</b></td>
    <td><b>relu</b></td>
    <td><img src="../assets/2/vld_dxy.png"/></td>
    <td><img src="../assets/2/vld_dxy_err.png"/></td>
  </tr>
    </tr>
    <tr>
    <td><b>relu</b></td>
    <td><b>trainable softplus N = 0.003811 B = -4.898549 </b></td>
    <td><img src="../assets/3/vld_dxy.png"/></td>
    <td><img src="../assets/3/vld_dxy_err.png"/></td>
  </tr>
</table>
&nbsp;

#### Summary of Preliminary Results
* 0\. **tanh**($`d_{xy}`$) inner activation layer | **relu**($`d_{xy}`$) output activation layer **CONTROL**
     * 18.181199710977403, 20.717629772152744, 16.46431748470159, 15.053283217335373, 18.532951547124313
* 1\. **tanh**($`d_{xy}`$) inner activation layer | **trainable softplus**($`d_{xy}`$) output activation layer
     * 18.311166383866556, 20.415174882727456, 16.280270881754124, 15.095944006116866, 18.792199922383546
* 2\. **relu**($`d_{xy}`$) inner activation layer | **relu**($`d_{xy}`$) output activation layer
     * 17.76940641948171, 18.782025167137814, 15.117155370859091, 14.409918326296124, 18.525291403957265
* 3\. **relu**($`d_{xy}`$) inner activation layer | **trainable softplus**($`d_{xy}`$) output activation layer
     * 18.357753423686862, 19.281630231126417, 14.776722825987486, 13.655147205616187, 19.42583351061187   
&nbsp;

Evidently, relu/relu (model 2) performed the best, so I chose to utilize relu as the output activation layer rather than the trainable softplus layer. This layer was retained in the layers directory of ucv5, in case it becomes necessary to return back to the concept.

&nbsp;

### Secondary Testing (for dxy only)
* After observing that $`p_{t}`$ and $`d_{xy}`$ inner activation functions can be disparate, and stitched together in the final neural network, I decided to experiment further with possible inner activation functions. For these, I determined **relu** to be the optimal output activation layer. 
* The following tests have these characteristics:
     * $`p_{t}`$: 
           * output activation layer: **coth**  
               * Note: this enables conversion from training on inverse $`q \cdot p_{t}`$ to the proper output   
           * inner activation layer: **tanh**
     * $`d_{xy}`$: 
           * output activation layer: **relu**
           * inner activation layer: *variable*              
&nbsp;
* Continuing with the previous numbering system, the following are the tested permutations of activation functions for the **hidden layers** for $`d_{xy}`$, where **relu** is the output activation layer:      
&nbsp;
* 0\. 🔴 **tanh**($`d_{xy}`$) inner activation (hyperbolic tangent) **CONTROL**
     * $`tanh(x) = (e^{2x}-1)/(e^{2x}+1)`$
* 2\. 🔵 **relu**($`d_{xy}`$) inner activation (rectified linear unit)
     * if $`x ≥ 0`$: return $`x`$
     * if $`x < 0`$: return $`0`$
* 4\. 🟢 **gelu**($`d_{xy}`$) inner activation (gaussian error linear unit)
     * $`gelu(x) = x/2 * (1 + erf(x)/sqrt(2))`$
     * where erf($`x`$) is the Gauss Error Function, determined by integating over the normal distribution
* 5\. 🟣 **selu**($`d_{xy}`$) inner activation (scaled exponential linear unit)
     * if $`x ≥ 0`$: return $`s * x`$
     * if $`x < 0`$: return $`s * \alpha * (exp(x) - 1)`$
     * where $`s = 1.05070098`$ and $`\alpha = 1.67326324`$
* 6\. ⚫ **softplus**($`d_{xy}`$) inner activation
     * $`softplus(x) = 1/N * (1 + exp(Nx)`$
* 7\. 🟠 **elu**($`d_{xy}`$) inner activation (exponential linear unit)
     * if $`x ≥ 0`$: return $`x`$
     * if $`x < 0`$: return $`(exp(x) - 1)`$
&nbsp;
<img src="../assets/desmos-graph.png"/>  

**Above: Activation Functions considered for use in ucv5**  

The following plots were generated for the models with the activation functions listed above, with 150 epochs and a batch size of 4096.
&nbsp;

#### Predicted Distribution and Error
<table>
  <tr>
    <th>Inner $`d_{xy}`$ Activation Layer</th>
    <th>$`d_{xy}`$ Predicted</th>
    <th>$`d_{xy}`$ Inclusive Error</th>
  </tr>
  <tr>
    <td><b>0. tanh</b></td>
    <td><img src="../assets/0/dxy_pred.png"/></td>
    <td><img src="../assets/0/dxy_0_to_inf.png"/></td>
  </tr>
    <tr>
    <td><b>2. relu</b></td>
    <td><img src="../assets/2/dxy_pred.png"/></td>
    <td><img src="../assets/2/dxy_0_to_inf.png"/></td>
  </tr>
  <tr>
    <td><b>4. gelu</b></td>
    <td><img src="../assets/4/dxy_pred.png"/></td>
    <td><img src="../assets/4/dxy_0_to_inf.png"/></td>
  </tr>
  <tr>
    <td><b>5. selu</b></td>
    <td><img src="../assets/5/dxy_pred.png"/></td>
    <td><img src="../assets/5/dxy_0_to_inf.png"/></td>
  </tr>
    <td><b>6. softplus</b></td>
    <td><img src="../assets/6/dxy_pred.png"/></td>
    <td><img src="../assets/6/dxy_0_to_inf.png"/></td>
  </tr>
  </tr>
    <td><b>7. elu</b></td>
    <td><img src="../assets/7/dxy_pred.png"/></td>
    <td><img src="../assets/7/dxy_0_to_inf.png"/></td>
  </tr>
</table>
&nbsp;

#### Validation and Error
<table>
  <tr>
    <th>Inner $`d_{xy}`$ Activation Layer</th>
    <th>$`d_{xy}`$ Correlation</th>
    <th>$`d_{xy}`$ Error</th>
  </tr>
  <tr>
    <td><b>0. tanh</b></td>
    <td><img src="../assets/0/vld_dxy.png"/></td>
    <td><img src="../assets/0/vld_dxy_err.png"/></td>
  </tr>
  <tr>
    <td><b>2. relu</b></td>
    <td><img src="../assets/2/vld_dxy.png"/></td>
    <td><img src="../assets/2/vld_dxy_err.png"/></td>
  </tr>
  <tr>
    <td><b>4. gelu</b></td>
    <td><img src="../assets/4/vld_dxy.png"/></td>
    <td><img src="../assets/4/vld_dxy_err.png"/></td>
  </tr>
  <tr>
    <td><b>5. selu</b></td>
    <td><img src="../assets/5/vld_dxy.png"/></td>
    <td><img src="../assets/5/vld_dxy_err.png"/></td>
  </tr>
  <tr>
    <td><b>6. softplus</b></td>
    <td><img src="../assets/6/vld_dxy.png"/></td>
    <td><img src="../assets/6/vld_dxy_err.png"/></td>
  </tr>
  <tr>
    <td><b>7. elu</b></td>
    <td><img src="../assets/7/vld_dxy.png"/></td>
    <td><img src="../assets/7/vld_dxy_err.png"/></td>
  </tr>
</table>
&nbsp;

#### Summary of Secondary Results
* 0\. **tanh**($`d_{xy}`$) inner activation (hyperbolic tangent) **CONTROL**
     * 18.181199710977403, 20.717629772152744, 16.46431748470159, 15.053283217335373, 18.532951547124313
* 2\. **relu**($`d_{xy}`$) inner activation (rectified linear unit)
     * 17.76940641948171, 18.782025167137814, 15.117155370859091, 14.409918326296124, 18.525291403957265
* 4\. **gelu**($`d_{xy}`$) inner activation (gaussian error linear unit)
     * 17.68765537656044, 20.23459375864538, 16.220472725701043, 14.670824028994764, 17.9843097871432
* 5\. **selu**($`d_{xy}`$) inner activation (scaled exponential linear unit)
     * 18.586557458964037, 21.67116775633407, 17.490534664667024, 15.802254677825378, 18.705092204339586
* 6\. **softplus**($`d_{xy}`$) inner activation
     * 19.150022184687455, 21.543195057978657, 17.112830403566978, 15.509048859107866, 19.64955914604092
* 7\. **elu**($`d_{xy}`$) inner activation (exponential linear unit)
     * 18.608460495536995, 21.11079735084918, 17.228504058115238, 15.828388849994555, 18.86890871139068
&nbsp;

### Final Model Predictions
* Models 2(**relu**) and 4(**gelu**) appeared to perform the best.  
* In particular, **relu** performed well for low-$`d_{xy}`$ events, and **gelu** performed well for high-$`d_{xy}`$ events, judging by the RMSE of each bin.
* Before stitching, a new boolean flag was included in the shell script used to change the training analyzer. This enables both analyzers to be used, in order to produce unified plots along with errors for $`p_{t}`$ and $`d_{xy}`$ calculated across their respective bins
* As a result, $`d_{xy}`$ models 0, 2, and 4 were analyzed, along with the $`p_{t}`$ model and ran with 300 epochs. 
* Updates to $`p_{t}`$:
      * While a split-activation function may be able to be implemented in design, this may not be practical even if it reduces memory allocation and saves a few lookups.
      * As a result, I also considered how the neural network would fare if **relu** was used as the activation function for the $`p_{t}`$ weights, so this is listed on the plots below.
          * $`q \cdot p_{t}`$ Model 0: **coth** output activation, **tanh** inner activation
          * $`q \cdot p_{t}`$ Model 1: **coth** output activation, **relu** inner activation
&nbsp;
* The following plots were generated using 300 epochs and a batch size of 2048.
&nbsp;

#### dxy Learning Rate, Validation, Error
<table>
  <tr>
    <th>$`d_{xy}`$ Inner Activation Function</th>
    <th>$`d_{xy}`$ Learning Rate</th>
    <th>$`p_{t}`$ Correlation</th>
    <th>$`p_{t}`$ Error</th>
  </tr>
  <tr>
    <td><b>tanh (ucv4)</b></td>
    <td><img src="../assets/10/learning_rate.png"/></td>
    <td><img src="../assets/10/dxy__vld_dxy.png"/></td>
    <td><img src="../assets/10/dxy__vld_dxy_err.png"/></td>
  </tr>
    <tr>
    <td><b>relu (ucv5)</b></td>
    <td><img src="../assets/11/learning_rate.png"/></td>
    <td><img src="../assets/11/dxy__vld_dxy.png"/></td>
    <td><img src="../assets/11/dxy__vld_dxy_err.png"/></td>
  </tr>
    <tr>
    <td><b>gelu (ucv5)</b></td>
    <td><img src="../assets/12/learning_rate.png"/></td>
    <td><img src="../assets/12/dxy__vld_dxy.png"/></td>
    <td><img src="../assets/12/dxy__vld_dxy_err.png"/></td>
  </tr>
</table>

#### Conclusions for dxy
* Clearly, the $`q \cdot p_{t}`$ prediction has higher efficiency for both low $`p_{t}`$ and high $`p_{t}`$ muons, and so it is an area for further investigation.
* The following are the errors for the 3 candidate $`d_{xy}`$ models for ucv_5 when trained over 300 epochs and a batch size of 2048
* 0\. **tanh**($`d_{xy}`$) inner activation (hyperbolic tangent) **CONTROL**
     * 17.6554968354019, 19.8310001377701, 16.104653487691184, 15.19400829410122, 17.94316218777603
* 2\. **relu**($`d_{xy}`$) inner activation (rectified linear unit)
     * 17.733263962461937, 20.963087663079886, 16.48079141070392, 14.966872056946029, 17.841228264306466
* 4\. **gelu**($`d_{xy}`$) inner activation (gaussian error linear unit)
     * 17.510400887697408, 20.326163248916263, 16.362621636448036, 14.925535812588562, 17.64872930046705
&nbsp;
* All of these errors and the plots above show that changes in the internal activation layer have little effect on $`d_{xy}`$ accuracy, although it is notable that **relu** performs at the same level as **tanh**, and may be easier to implement in practicality since a lookup table may not need to be used.
* If improvement to the model is desired, utilizing **Gaussian Error Linear Unit(gelu)** as the activation function may be the most promising avenue. 

#### pT Learning Rate, Validation, Error
<table>
  <tr>
    <th><b>Model</b></th>
    <th>$`p_{t}`$ Correlation</th>
    <th>$`p_{t}`$ Error</th>
  </tr>
  <tr>
    <td><b>ucv4 $`p_{t}`$</b></td>
    <td><img src="../../ucv4/assets/ucv4/vld_pt.png"/></td>
    <td><img src="../../ucv4/assets/ucv4/vld_pt_err.png"/></td>
  </tr>
  <tr>
    <td><b>ucv5 $`q \cdot p_{t}`$ with tanh</b></td>
    <td><img src="../../ucv5/assets/ucv5_invpt_tanh/vld_qpt.png"/></td>
    <td><img src="../../ucv5/assets/ucv5_invpt_tanh/vld_qpt_err.png"/></td>
  </tr>
    <tr>
    <td><b>ucv5 $`q \cdot p_{t}`$ with relu</b></td>
    <td><img src="../../ucv5/assets/ucv5_invpt_relu/vld_qpt.png"/></td>
    <td><img src="../../ucv5/assets/ucv5_invpt_relu/vld_qpt_err.png"/></td>
  </tr>
</table>

#### pT Efficiency
<table>
  <tr>
    <th><b>Model</b></th>
    <th>$`p_{t}`$ Efficiency</th>
  </tr>
  <tr>
    <td><b>ucv4 $`p_{t}`$</b></td>
    <td><img src="../../ucv4/assets/ucv4/vld_eff_vs_pt.png"/></td>
  </tr>
  <tr>
    <td><b>ucv5 $`q \cdot p_{t}`$ with tanh</b></td>
    <td><img src="../../ucv5/assets/ucv5_invpt_tanh/vld_eff_vs_pt.png"/></td>
  </tr>
    <tr>
    <td><b>ucv5 $`q \cdot p_{t}`$ with relu</b></td>
    <td><img src="../../ucv5/assets/ucv5_invpt_relu/vld_eff_vs_pt.png"/></td>
  </tr>
</table>

#### Conclusions for pT
* And the following are the inverse errors for the 2 candidate $`q \cdot p_{t}`$ models:
     * These errors are grouped into four bins based on $`p_{t}`$, instead of the 5 for $`d_{xy}`$
          * (0-inf), (0-10), (10-20), (20-inf)
* 0\. **tanh**($`q \cdot p_{t}`$) inner activation (hyperbolic tangent) **CONTROL**
     * qinvpt error: 0.10293881949866138, 0.11122999058628176, 0.07752705664583921, 0.08080708248554287
* 1\. **relu**($`q \cdot p_{t}`$) inner activation (rectified linear unit)
     * qinvpt error: 0.1109730865124782, 0.12134437859245961, 0.08048567965758711, 0.08003124846195564
* Clearly, the model's error suffer a bit, but the validation plots show almost no change
     * Visually, the change does not neccesarily match the hypothetical extreme effect of zeroing out any internal negative weights (as **relu** would do), possibly because of the batch normalization that follows the activation layer. 
* However, the efficiency plot shows a noticeable improvement at the plateau region in switching from **tanh** to **relu**
* In short, it may be possible to just use **relu** internally for both $`p_{t}`$ and $`d_{xy}`$, but this does come at the cost of slightly increased error. For this to be considered, more studies should be done to see if this is viable. 

#### Final Stitching (relu)
* After recognizing that using relu for $`q \cdot p_{t}`$ improves the efficiency plot, I just stitched the models together and called both the  $`q \cdot p_{t}`$ and $`d_{xy}`$ training analyzers. The errors for this model are listed below (note that the bins are different for dxy and for qinvpt)
* dxy_err_by_dxy_bin rmse: 17.527801475130833, 19.87011268879498, 15.862954077553653, 14.675992354327558, 17.861518012605995
* qinvpt_err rmse: 0.11097308657163478, 0.12134437870104677, 0.0804856794454394, 0.08003124843820661

**Resources:**    
[https://alaaalatif.github.io/2019-04-11-gelu/](https://alaaalatif.github.io/2019-04-11-gelu/)<br />
[https://paperswithcode.com/method/gelu](https://paperswithcode.com/method/gelu)<br />
[https://himanshuxd.medium.com/activation-functions-sigmoid-relu-leaky-relu-and-softmax-basics-for-neural-networks-and-deep-8d9c70eed91e](https://himanshuxd.medium.com/activation-functions-sigmoid-relu-leaky-relu-and-softmax-basics-for-neural-networks-and-deep-8d9c70eed91e)<br />
[https://www.tensorflow.org/api_docs/python/tf/keras/activations/elu](https://www.tensorflow.org/api_docs/python/tf/keras/activations/elu)<br />

