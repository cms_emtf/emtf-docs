# ucv5.1

- Author: Mason Weiss
- Last Updated: 25 July 2023
- Project Start Date: 5 July 2023 

### Goal
* Build upon the *ucv4* model, which was implemented at P5 in June 2023
* Refer to discoveries from the original *ucv5* model I worked on in the spring, see https://cms-emtf.docs.cern.ch/emtf/run3nn/ucv5/ucv5/

### Summary of Changes
1. Implemented $q \cdot p_{t}$ training (begin with *ucv5* model from March):
      * **relu** inner activation layer
      * Hybrid output activation layers
          * $ \boldsymbol a/x + \boldsymbol bx \pm \boldsymbol c$ for $q \cdot p_{t}$ output activation layer
               * $ \boldsymbol a , \boldsymbol b , \boldsymbol c$ are treated as trainable weights, and ***c*** acts as a symmetric bias
          * $ \boldsymbol ax \pm \boldsymbol c$ for $d_{0}$ output activation layer
               * $ \boldsymbol a , \boldsymbol c$ are treated as trainable weights, and ***c*** acts as a symmetric bias
          * These are stitched together seamlessly in the final model so that raw outputs from the last dense layer of the $q \cdot p_{t}$ portion recieve the $q \cdot p_{t}$ activation function, and the other half recieves $d_{0}$ activation.  
      * Retained the same structure from ucv4, so two loss functions required
      * train $p_{t}$ component of NN by calculating loss on $q/p_{t}$; loss function remaind as logcosh
          * entire range of $q \cdot p_{t}$ then can be generated through the output activation layer of **coth**
      * use logcosh for $d_{xy}$
&nbsp;
2. Fold in first batch normalization layer
      * hls4ml automatically folds in batch normalization layers, so long as they exist after a densely connected one
      * However, in this model, a batch normalization layer is used as a pre-processing layer to normalize the inputs, meaning that the conversion is not implicit in hls4ml
      * As such, Patrick worked to generate code to fold in a batch norm with the following dense layer, and I've implemented the general function into ucv5.1 as follows:
      $$y = \frac{\gamma(x-\mu)}{\sqrt{\sigma^{2}+\varepsilon}} + \beta$$
      * Where:
          * $\mu$ represents the moving mean, and is not trainable 
          * $\sigma^{2}$ represents the moving variance, and is not trainable
          * $\gamma$ represents a scale factor multiplied by the scaled input
          * $\beta$ represents a shift added to the scaled quantity
      * Important here is that the equation can be simplified in inference, since the "moving" mean and variance are fixed post-training, so we can now simplify the batch normalization to be a linear equation:
         $$y = s(x- \mu) + \beta$$
         simpifies to
         $$y = sx + \omega$$
      * Where:
         $$s = \frac{\gamma}{\sqrt{\sigma^{2}+\varepsilon}}$$
         $$k = \beta - s\mu$$
      * $s$ and $k$ can then be used to recompute the new kernel $W'$ and bias vector $b'$  of the following dense layer as follows:
         $$W_{ij}' = W_{ij} \cdot s_{i}$$
         * where $W_{ij}'$ denotes the new weight connecting input $i$ to dense node $j$, and $s_{i}$ denotes the scale factor given input $i$ 
         $$b_{j}' = \sum_{i}^{i_{max}}(W_{ij} \cdot k_{i})$$
         * where $b_{j}'$ denotes the $j^{\text {th}}$ element of the bias vector, and $s_{i}$
      * $W'$ and $b'$ can then be used to construct the new first dense layer

3. Handle activation output
     * We then determined that it would be ideal to create a LUT to handle the output activation functions, since this could feasibly enable calibration as well
     * For $q \cdot p_{t}$, $\boldsymbol a/x + \boldsymbol bx \pm \boldsymbol c$ using weights of $[\boldsymbol a, \boldsymbol b, \boldsymbol c]$ where all are trainable
     * For $d_{0}$, $\boldsymbol ax \pm \boldsymbol c$ using weights of $[\boldsymbol a, \boldsymbol c]$ where all are trainable
     * In implementation, the final activation layer could be removed and the LUT could be applied to the final dense layer output

4. Quantize weights to mimic firmware behavior given DSP specifications
     * Much time was spent on this, using the layers built into QKeras which can act as drop-in replacements for Keras layers
        * a QKeras constraint can be used which stores the full-precision weights to implement quantized-aware training
     * The general workflow determined to be ideal was:
        1. train a full-precision model
        2. fold in the first batch normalization layer
        3. train a quantized model initialized with the full-precision model weights from (2)
        4. if necessary, copy the model from (3) and remove the final activation output and record the weights to construct a LUT
     * The quantizer constraint class was also used to create another version of a quantized neural network, where the quantizer was used as a constraint for the Keras dense layer weights
     * After comparing the output from HLS with the Keras model, it was determined that QKeras quantization served to be too aggressive of a method, and penalized the efficiency of the model more than reality.
     * Consequently, we elected to use the full-precision Keras model to mimic the performance in firmware, and not quantize the network

5. Fix activation output weights
      * After observing the performance of the model with the activation weights being trainable, no significant improvement was made by having trainable weights, as the weights of the dense layer output could simply adjust for the variation.
      * If the range of these weights could be assumed ahead of time, determining the address output of the network would be more reliable.
      * As such, the weights were fixed as [1, 0, 0] and [1, 0], effectively making the $q \cdot p_{t}$ activation a strict inverse, and the $d_{0}$ activation a pass-through

6. Size model given firmware and timing constraints
      * Following folding the first batch normalization, and with the knowledge that hls4ml will fold in the internal batch normalization layers, it was determined
      * Initially, we attempted to increase from two internal dense layers with 10 and 8 nodes to three internal dense layers with 20, 15, and 10 nodes.
      * This did not fit in timing, and in an attempt to reduce FPGA LUT usage while maintaining the improvement in performance we settled on 16, 12, 8 nodes. 

7. Fix activation output parameters
      * the activation LUT could then be used not only for activation, but also for calibration in the same manner as for phase 2, and removing the sign for both outputs. 
      * Osvaldo designed an activation function that given an output bit width, [TBD]

8. Model Description (post-fold)
      *  $q \cdot p_{t}$ submodel:
      

&nbsp;
### Model Comparison
aaaaa

