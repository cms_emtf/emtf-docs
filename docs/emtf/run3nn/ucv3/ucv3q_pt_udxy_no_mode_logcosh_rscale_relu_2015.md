# ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015

- Author: Osvaldo Miguel Colin
- Last Updated: 17/March/2023

Loss Function

- Logcosh

Parameter Weight

This was recently introduced.
Tensorflow's loss function implementations calculate the loss for each parameter and then avareges the losses for each set of inputs.
I rewrote this to include a weighted average by introducing the weights below.

- $`f_{p_{T}}=0.8`$
- $`f_{d_{xy}}=0.2`$

Event Weight

This was recently introduced. There are cases where we wish to optimize the training to favor certain features of the output.
Usually this is done by weighing the events. Here I used a function that favors low $`p_T`$ events the most,
then treats the following range equally, and finally reduces the importance of $`p_T`$ beyond the highest $`p_T`$ threshold (22 GeV).

$`f(p_T) = N * ((1 + Tanh(4 - p_T/10)) + 100/{p_T^2})`$

Here $`N`$ is a normalization constant. Given that the sample I used for this training is flat in $`1/p_T`$, 
it's necessary to first weigh it so that it's flat in $`p_T`$ before applying $`f(p_T)`$.

The full expression for the weights is shown below:

$`w(p_T) = {p_T^2 \over k} * N * ((1 + tanh(4 - p_T/10)) + 100/{p_T^2})`$

$`N`$ ensures that the $`w(p_T)*g(p_T)`$ integrates to unity from 2 to 120 GeV.
$`k`$ is defined as the probability of getting any $`1/p_T`$ value in the original flat distribution.

$`k={1 \over {1/2-1/120}}`$

![](assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/weight_distribution.png)

The figure above shows the original distribution (Blue), and the weighted distribution (Orange). 
The x-axis shows the $`p_T`$ and the y-axis shows the probability for that $`p_T`$.
The average loss of the events is weighted using $`w(p_T)`$.

Internal Layers

- Dense 20 Node Layer
- Dense 15 Node Layer

Variables (Input)

- Removed x_track_mode
- Set NAN to 0 for dphi
- Set NAN to 0 for dtheta

Parameters

- Unsigned $`d_{xy}`$

Predictions

<table>
  <tr>
    <th>Type</th>
    <th>$`p_{T}`$</th>
    <th>$`d_{xy}`$</th>
  </tr>
  <tr>
    <td>True</td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/pt_true.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/dxy_true.png"/></td>
  </tr>
  <tr>
    <td>Predicted</td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/pt_pred.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/dxy_pred.png"/></td>
  </tr>
</table>

RMSE

<table>
  <tr>
    <th>Type</th>
    <th>$`p_{T}`$</th>
    <th>$`p_{T}`$</th>
    <th>$`1/p_{T}`$</th>
    <th>$`d_{xy}`$</th>
  </tr>
  <tr>
    <td>Inclusive</td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/pt_0_to_inf.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/pt_rel_0_to_inf.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/invpt_0_to_inf.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/dxy_0_to_inf.png"/></td>
  </tr>
  <tr>
    <td>$`0 - 10\ \text{GeV}`$</td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/pt_0_to_10.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/pt_rel_0_to_10.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/invpt_0_to_10.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/dxy_0_to_10.png"/></td>
  </tr>
  <tr>
    <td>$`10 - 20\ \text{GeV}`$</td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/pt_10_to_20.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/pt_rel_10_to_20.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/invpt_10_to_20.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/dxy_10_to_20.png"/></td>
  </tr>
  <tr>
    <td>$`20 - \inf\ \text{GeV}`$</td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/pt_20_to_inf.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/pt_rel_20_to_inf.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/invpt_20_to_inf.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/dxy_20_to_inf.png"/></td>
  </tr>
</table>

Correlations

<table>
  <tr>
    <th>Type</th>
    <th>$`p_{T}`$</th>
    <th>$`d_{xy}`$</th>
  </tr>
  <tr>
    <td>Prediction</td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/vld_pt.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/vld_dxy.png"/></td>
  </tr>
  <tr>
    <td>Error</td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/vld_pt_err.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/vld_dxy_err.png"/></td>
  </tr>
</table>

Efficiencies

<table>
  <tr>
    <th>Variable</th>
    <th>$`p_{T}`$</th>
    <th>$`d_{xy}`$</th>
    <th>$`\eta^{*}`$</th>
  </tr>
  <tr>
    <td>$`p_{T}`$</td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/vld_eff_vs_pt.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/vld_eff_vs_dxy.png"/></td>
    <td><img src="../assets/ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015/vld_eff_vs_eta_star.png"/></td>
  </tr>
</table>

