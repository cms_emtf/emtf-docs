# ucv2_pt_udxy_relu_2015

- Author: Osvaldo Miguel Colin
- Last Updated: 17/March/2023

Internal Layers

- Dense 20 Node Layer
- Dense 15 Node Layer

Parameters

- Unsigned $`d_{xy}`$

Predictions

<table>
  <tr>
    <th>Type</th>
    <th>$`p_{T}`$</th>
    <th>$`d_{xy}`$</th>
  </tr>
  <tr>
    <td>True</td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/pt_true.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/dxy_true.png"/></td>
  </tr>
  <tr>
    <td>Predicted</td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/pt_pred.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/dxy_pred.png"/></td>
  </tr>
</table>

RMSE

<table>
  <tr>
    <th>Type</th>
    <th>$`1/p_{T}`$</th>
    <th>$`p_{T}`$</th>
    <th>$`p_{T}`$</th>
    <th>$`d_{xy}`$</th>
  </tr>
  <tr>
    <td>Inclusive</td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/pt_0_to_inf.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/pt_rel_0_to_inf.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/invpt_0_to_inf.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/dxy_0_to_inf.png"/></td>
  </tr>
  <tr>
    <td>$`0 - 10\ \text{GeV}`$</td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/pt_0_to_10.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/pt_rel_0_to_10.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/invpt_0_to_10.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/dxy_0_to_10.png"/></td>
  </tr>
  <tr>
    <td>$`10 - 20\ \text{GeV}`$</td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/pt_10_to_20.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/pt_rel_10_to_20.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/invpt_10_to_20.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/dxy_10_to_20.png"/></td>
  </tr>
  <tr>
    <td>$`20 - \inf\ \text{GeV}`$</td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/pt_20_to_inf.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/pt_rel_20_to_inf.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/invpt_20_to_inf.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/dxy_20_to_inf.png"/></td>
  </tr>
</table>

Correlations

<table>
  <tr>
    <th>Type</th>
    <th>$`p_{T}`$</th>
    <th>$`d_{xy}`$</th>
  </tr>
  <tr>
    <td>Prediction</td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/vld_pt.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/vld_dxy.png"/></td>
  </tr>
  <tr>
    <td>Error</td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/vld_pt_err.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/vld_dxy_err.png"/></td>
  </tr>
</table>

Efficiencies

<table>
  <tr>
    <th>Variable</th>
    <th>$`p_{T}`$</th>
    <th>$`d_{xy}`$</th>
    <th>$`\eta^{*}`$</th>
  </tr>
  <tr>
    <td>$`p_{T}`$</td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/vld_pt_eff.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/vld_pt_eff_vs_dxy.png"/></td>
    <td><img src="../assets/ucv2_pt_udxy_relu_2015/vld_pt_eff_vs_eta_star.png"/></td>
  </tr>
</table>

