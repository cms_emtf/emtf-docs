# Run3 NN

- Author: Osvaldo Miguel Colin
- Last Updated: 17/March/2023

Run3 NN model studies using converted and unconverted inputs. 

## Maintainers

- Osvaldo Miguel Colin (om15@rice.edu)
- Mason Weiss (mw103@rice.edu)
- Sergo Jindariani
- Efe Yigitbasi

## Repositories

- [Repository](https://gitlab.com/rice-acosta-group/emtf/emtf-neural-network) EMTF Neural Network

## Status

- Latest model: [ucv4](ucv4/ucv4.md)

## Change Log

- 23/Mar/2023
    - Added new experimental model to predict $`q \cdot p_{t}`$ and better predict $`d_{xy}`$: ucv5
- 17/Mar/2023
    - Added new model: ucv4
- 03/Dec/2022
    - Added new model: ucv3q_pt_udxy_no_mode_logcosh_rscale_relu_2015
- 06/Nov/2022
    - Added new performance plots.
    - Added efficiency plots.
    - Redesigned the Run3 NN layout.
- 27/Oct/2022
    - Fixed 2 trainings: ucv2_pt_udxy_fwmax_relu_2015 and ucv2_pt_udxy_no_mode_fwmax_relu_2015.
    - dphi and dtheta were using 14 and 8 bits respectively for max value, should have been 13 and 7 bits.
- 24/Oct/2022
    - Added 2 new trainings: ucv2_pt_udxy_fwmax_relu_2015 and ucv2_pt_udxy_no_mode_fwmax_relu_2015.
- 16/Oct/2022
    - Reran training for unconverted models since there was a bug in the mode selection for the unconverted inputs v1.
      The bug has been fixed and the unconverted inputs from v1 and v2 now match.
    - Added 2 new trainings: ucv2_pt_udxy_relu_2015 and ucv2_pt_udxy_no_mode_relu_2015.
      ucv2_pt_udxy_no_mode_relu_2015 is used to crosscheck uc_pt_udxy_nfr_nme11_relu_2015.
    - Moved comparisons to it's own section.
    - Moved code to it's own section.

