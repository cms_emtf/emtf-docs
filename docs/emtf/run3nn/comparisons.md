# RMSE Comparisons

- Author: Osvaldo Miguel Colin
- Last Updated: 17/March/2023

$`p_T`$ (RMSE)

![](assets/pt_rmse.png)

$`p_T`$ (RMSRE)

![](assets/pt_rmsre.png)

$`1 \over p_T`$

![](assets/invpt_rmse.png)

$`d_{\text{xy}}`$

![](assets/dxy_rmse.png)


## Comments

- 06/Nov/2022 - Osvaldo
    - Fixed bug in performance plots where training inputs were used for performance measurement in the last 2 trainings. 
    - Fixed bug in $`1/p_{T}`$ error where 0 $`p_{T}`$ prediction made the error diverge.
- 27/Oct/2022 - Osvaldo
    - Fixed a bug that used 14 and 8 bits for the max value for dphi and dtheta respectively.
    - The models that use the max value allowed in fw, perform slightly worse than the models where 0 is used for the invalid dphi and dtheta values.
- 24/Oct/2022 - Osvaldo
    - Setting the invalid dphi and dtheta values to the maximum values allowed in firmware worsens the resolution for dxy, however pt resolution is not affected.
    - It should be noted that with and without track mode the results are the same.
