# CMS Muon Endcap Track Finder DAQ readout format

- Author: A. Madorsky, University of Florida/Physics
- Date: 2022-08-25

> Note: This file has been converted to markdown by Osvaldo using pandoc. 
> In case of inconsistencies please refer to the original file by Alex: [DOCX](assets/daq/EMU_TF_DAQ_format.docx)

The format is designed to easily interface with a 64-bit readout system through AMC13 card. This requires that all data sent to the AMC13 be an integer multiple of 64-bit words.

The Event Record has the following structure:

**Table 1: Event record structure**

<table>
  <colgroup>
    <col style="width: 33%" />
    <col style="width: 66%" />
  </colgroup>
  <thead>
    <tr class="header">
      <th><strong>Name</strong></th>
      <th><strong>Size, 64-bit words</strong></th>
    </tr>
  </thead>
  <tbody>
    <tr class="odd">
      <td>Event Record Header</td>
      <td>3</td>
    </tr>
    <tr class="even">
      <td>MPC link error word</td>
      <td>1</td>
    </tr>
    <tr class="odd">
      <td>ME Track segments data</td>
      <td>Variable, depends on presence of valid track segments and number of time bins requested</td>
    </tr>
    <tr class="even">
      <td>RPC primitives data</td>
      <td>Variable, depends on presence of valid RPC primitives and number of time bins requested</td>
    </tr>
    <tr class="odd">
      <td>GEM primitives data</td>
      <td>Variable, depends on presence of valid GEM primitives and number of time bins requested</td>
    </tr>
    <tr class="even">
      <td>SP track output data</td>
      <td>Variable, depends on presence of detected tracks and number of time bins requested</td>
    </tr>
    <tr class="odd">
      <td>Event record trailer</td>
      <td>2</td>
    </tr>
  </tbody>
</table>

Table below lists maximum SP Event Record sizes in 64-bit words for 0 to 7 Time Bins.

**Table 2: Maximum SP Event Record sizes for 0 to 7 TBIN values**

<table>
  <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
  </colgroup>
  <thead>
    <tr class="header">
      <td rowspan="2"><strong>TBIN</strong></td>
      <td><strong>64-bit</strong></td>
    </tr>
    <tr class="header">
      <td><strong>Words</strong></td>
    </tr>
  </thead>
  <tbody>
    <tr class="even">
      <td>0</td>
      <td>18</td>
    </tr>
    <tr class="odd">
      <td>1</td>
      <td>328</td>
    </tr>
    <tr class="even">
      <td>2</td>
      <td>638</td>
    </tr>
    <tr class="odd">
      <td>3</td>
      <td>948</td>
    </tr>
    <tr class="even">
      <td>4</td>
      <td>1258</td>
    </tr>
    <tr class="odd">
      <td>5</td>
      <td>1568</td>
    </tr>
    <tr class="even">
      <td>6</td>
      <td>1878</td>
    </tr>
    <tr class="odd">
      <td>7</td>
      <td>2188</td>
    </tr>
  </tbody>
</table>

Maximum Event Record size is 6 64-bit words for Headers/Counters/Trailers plus 120 64-bit words per a TBIN. For TBIN=7, event size comes to 2188 64-bit words in total. At 10 Gbps DAQ line rate, the maximum size DAQ block will take 27.35 uS to transmit. Note that vast majority of the DAQ blocks will be much smaller (order of magnitude or more) because for a typical event just a small fraction of track segments is valid.

The main DAQ path in the upgraded Trigger system is AMC13 board. These boards receive information in 64-bit words. The tables in this document are organized in 16-bit notation. Each table contains a multiple of 4 16-bit words. The output 64-bit words are composed from 16-bit words as shown below:

**Table 3: 64-bit word representation**

<table>
  <colgroup>
    <col style="width: 41%" />
    <col style="width: 14%" />
    <col style="width: 14%" />
    <col style="width: 14%" />
    <col style="width: 14%" />
  </colgroup>
  <thead>
    <tr class="header">
      <th>Bits</th>
      <th>[63:48]</th>
      <th>[47:32]</th>
      <th>[31:16]</th>
      <th>[15:0]</th>
    </tr>
  </thead>
  <tbody>
    <tr class="odd">
      <td>16-bit word index</td>
      <td>d</td>
      <td>c</td>
      <td>b</td>
      <td>a</td>
    </tr>
  </tbody>
</table>

Bit 15 of each 16-bit word is used as section identifier. In each resulting 64-bit DAQ word, these bits (numbers 63, 47, 31, 15) form a unique ID code that should be used to identify the section type.

## AMC data header

Each AMC in uTCA chassis must provide a header and a trailer according to this document, to satisfy AMC13 receiver:

<http://ohm.bu.edu/~hazen/CMS/AMC13/UpdatedDAQPath_2014-07-10.pdf>

MTF7 will form the AMC13 header and trailer as required.

## Event Record Header

The Event Record Header consists of 3 64-bit words, where the most significant hex digit in each 16-bit word is the legacy DDU Code word 0x9 and 0xA. Green cells in [Table 2] carry the SP-specific configuration settings; tan cells carry the SP-specific status; the content of all other cells complies with the legacy DDU requirement.

**Table 4: Event Record Header**

<table style="width:100%;">
  <colgroup>
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
  </colgroup>
  <thead>
    <tr class="header">
      <th><strong>D15</strong></th>
      <th><strong>D14</strong></th>
      <th><strong>D13</strong></th>
      <th><strong>D12</strong></th>
      <th><strong>D11</strong></th>
      <th><strong>D10</strong></th>
      <th><strong>D9</strong></th>
      <th><strong>D8</strong></th>
      <th><strong>D7</strong></th>
      <th><strong>D6</strong></th>
      <th><strong>D5</strong></th>
      <th><strong>D4</strong></th>
      <th><strong>D3</strong></th>
      <th><strong>D2</strong></th>
      <th><strong>D1</strong></th>
      <th><strong>D0</strong></th>
      <th><strong>Word</strong></th>
    </tr>
  </thead>
  <tbody>
    <tr class="odd">
      <td colspan="4">0x9</td>
      <td colspan="12">L1A [11:0]</td>
      <td>HD1a</td>
    </tr>
    <tr class="even">
      <td colspan="4">0x9</td>
      <td colspan="12">L1A [23:12]</td>
      <td>HD1b</td>
    </tr>
    <tr class="odd">
      <td colspan="4">0x9</td>
      <td colspan="12">0</td>
      <td>HD1c</td>
    </tr>
    <tr class="even">
      <td colspan="4">0x9</td>
      <td colspan="12">L1A BXN [11:0]</td>
      <td>HD1d</td>
    </tr>
    <tr class="odd">
      <td colspan="4">0xA</td>
      <td colspan="5">GEM CRC MATCH[5:1]</td>
      <td colspan="7">GEM[7:1]</td>
      <td>HD2a</td>
    </tr>
    <tr class="even">
      <td colspan="4">0xA</td>
      <td colspan="4" style="background: #ffc000">SP_TS[3:0]</td>
      <td colspan="3" style="background: #ffc000">SP_ERSV[2:0]</td>
      <td colspan="5" style="background: #ffc000">SP_ADDR[4:0]</td>
      <td>HD2b</td>
    </tr>
    <tr class="odd">
      <td colspan="4">0xA</td>
      <td colspan="1" style="background: #ccff99">use_ nn_pt</td>
      <td colspan="3" style="background: #ccff99">TBIN[2:0]</td>
      <td style="background: #ccff99">ddm</td>
      <td style="background: #ccff99">spa</td>
      <td style="background: #ccff99">rpca</td>
      <td style="background: #ffc000">skip</td>
      <td style="background: #ffc000">rdy</td>
      <td style="background: #ffc000">bsy</td>
      <td style="background: #ffc000">osy</td>
      <td style="background: #ffc000">wof</td>
      <td>HD2c</td>
    </tr>
    <tr class="even">
      <td colspan="4">0xA</td>
      <td colspan="12" style="background: #ccff99">ME1a[12:1]</td>
      <td>HD2d</td>
    </tr>
    <tr class="odd">
      <td>1</td>
      <td colspan="4" style="background: #ccff99">CPPF[4:1]</td>
      <td colspan="2">GEM CRC MATCH [7:6]</td>
      <td colspan="9" style="background: #ccff99">ME1b[9:1]</td>
      <td>HD3a</td>
    </tr>
    <tr class="even">
      <td>0</td>
      <td>0</td>
      <td colspan="3" style="background: #ccff99">CPPF[7:5]</td>
      <td colspan="11" style="background: #ccff99">ME2[11:1]</td>
      <td>HD3b</td>
    </tr>
    <tr class="odd">
      <td>0</td>
      <td colspan="4">CPPF CRC MATCH[4:1]</td>
      <td colspan="11" style="background: #ccff99">ME3[11:1]</td>
      <td>HD3c</td>
    </tr>
    <tr class="even">
      <td>0</td>
      <td>0</td>
      <td colspan="3">CPPF CRC MATCH[7:5]</td>
      <td colspan="11" style="background: #ccff99">ME4[11:1]</td>
      <td>HD3d</td>
    </tr>
  </tbody>
</table>

Here:

- L1A [23:0] – Event Number picked from a 24-bit Event Counter;
- L1A_BXN [11:0] – Event Bunch Crossing Number (L1A arrival time) picked from a 12-bit Bunch Counter, running at TTC timing;
- SP_TS [3:0] – SP Trigger Sector 1, 2, 3, 4, 5, 6 for +Z EMU side and 7, 8, 9, 10, 11, 12 for –Z EMU side:
- SP_ERSV [2:0] = 0,…,7 – SP Event Record Structure Version;
    - SP_ERSV = 0 =&gt; initial draft 2014-07-15;
- SP_PADR [4:0] = 0..5 – SP Slot Number in uTCA chassis;
- DDM = 0 (default) / 1 – AMC13 (default) / PCIe Readout Mode.
- DD/CSR_DFC [10:0] = DAQ FIFO Configuration register:
    - RPCA = 0 / 1 (default) – RPC Active. If the bit is set to 1, then the RPC interface is considered to be ACTIVE;
    - SPA = 0 / 1 (default) – Sector Processor Active. If the bit is set to 1, then the Sector Processor output is considered to be ACTIVE and the DAQ block contains SP muon Track(s) SP1, SP2, SP3;
    - TBIN [2:0] = 0…7 (default = 4) – data collected from 0…7 Time Bins (bunch crossings).
- RDY, BSY, OSY, WOF – TTS signals Ready, Busy, Out-of-SYnch, Warning OverFlow.
- SKIP – if the SKIP bit is set to 1, then the Event Data is skipped for the current event, and the event shrinks to the Event Record Header, Block of Counters and Event Record Trailer only, as if the TBIN equals 0 (although actually not), see details on the L1A Finite State Machine (FSM) below.
- ME\* are enable flags for all Muon Endcap CSCs. The notation is shown below:
    - ME means Muon Endcap
    - 1a, 1b, 2, 3, 4 is station number
    - index at the end is the CSC ID.
- CPPF\* are enable flags for CPPF links. Index at the end is link number.
- CPPF CRC MATCH flags are set to 1 if CRC received from each CPPF link on each orbit matches calculated CRC. These flags are reported as they were at the first BX in each readout window.
- use_nn_pt is the configuration flag that determines whether PT_LUT_address of NN_PT are reported in the SP output data record

All TTS signals carry signal values at the time the current Event Record Header is composed, and NOT at the time L1A has been received. These signals are sent to TTS input of the AMC13 interface module.

## MPC link errors

**Table 5: MPC link errors**

<table>
  <colgroup>
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
    <col style="width: 5%" />
  </colgroup>
  <thead>
    <tr class="header">
      <th><strong>D15</strong></th>
      <th><strong>D14</strong></th>
      <th><strong>D13</strong></th>
      <th><strong>D12</strong></th>
      <th><strong>D11</strong></th>
      <th><strong>D10</strong></th>
      <th><strong>D9</strong></th>
      <th><strong>D8</strong></th>
      <th><strong>D7</strong></th>
      <th><strong>D6</strong></th>
      <th><strong>D5</strong></th>
      <th><strong>D4</strong></th>
      <th><strong>D3</strong></th>
      <th><strong>D2</strong></th>
      <th><strong>D1</strong></th>
      <th><strong>D0</strong></th>
      <th><strong>Word</strong></th>
    </tr>
  </thead>
  <tbody>
    <tr class="odd">
      <td>0</td>
      <td>S1C8</td>
      <td>S1C7</td>
      <td>S1C6</td>
      <td>S1C5</td>
      <td>S1C4</td>
      <td>S1C3</td>
      <td>S1C2</td>
      <td>S0C9</td>
      <td>S0C8</td>
      <td>S0C7</td>
      <td>S0C6</td>
      <td>S0C5</td>
      <td>S0C4</td>
      <td>S0C3</td>
      <td>S0C2</td>
      <td>ERa</td>
    </tr>
    <tr class="even">
      <td>1</td>
      <td>S3C7</td>
      <td>S3C6</td>
      <td>S3C5</td>
      <td>S3C4</td>
      <td>S3C3</td>
      <td>S3C2</td>
      <td>S2C9</td>
      <td>S2C8</td>
      <td>S2C7</td>
      <td>S2C6</td>
      <td>S2C5</td>
      <td>S2C4</td>
      <td>S2C3</td>
      <td>S2C2</td>
      <td>S1C9</td>
      <td>ERb</td>
    </tr>
    <tr class="odd">
      <td>0</td>
      <td>S5C5</td>
      <td>S5C4</td>
      <td>S5C3</td>
      <td>S5C2</td>
      <td>S5C1</td>
      <td>S4C9</td>
      <td>S4C8</td>
      <td>S4C7</td>
      <td>S4C6</td>
      <td>S4C5</td>
      <td>S4C4</td>
      <td>S4C3</td>
      <td>S4C2</td>
      <td>S3C9</td>
      <td>S3C8</td>
      <td>ERc</td>
    </tr>
    <tr class="even">
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>S4C1</td>
      <td>S3C1</td>
      <td>S2C1</td>
      <td>S1C1</td>
      <td>S0C1</td>
      <td>S5C9</td>
      <td>S5C8</td>
      <td>S5C7</td>
      <td>S5C6</td>
      <td>ERd</td>
    </tr>
  </tbody>
</table>

MPC link error flags’ states are reported as they were at the first BX in each readout window. The codes in Table 5 are SXCY, where:

-   X is station number: 0 = ME1a, 1 = ME1b, 2 = ME2, 3 = ME3, 4 = ME4, 5 = all chambers from neighbor sector;
-   Y is the chamber number: for ME1a,b and ME2,3,4 Y = CSCID, for neighbor sector see table below:

<table style="width:100%;">
  <colgroup>
    <col style="width: 25%" />
    <col style="width: 25%" />
    <col style="width: 25%" />
    <col style="width: 25%" />
  </colgroup>
  <thead>
    <tr class="header">
      <th><strong>CSCID in station 5 (Y)</strong></th>
      <th><strong>Neighbor sector station</strong></th>
      <th><strong>Neighbor sector subsector</strong></th>
      <th><strong>Neighbor sector CSCID</strong></th>
    </tr>
  </thead>
  <tbody>
    <tr class="odd">
      <td>1</td>
      <td>1</td>
      <td>2</td>
      <td>3</td>
    </tr>
    <tr class="even">
      <td>2</td>
      <td>1</td>
      <td>2</td>
      <td>6</td>
    </tr>
    <tr class="odd">
      <td>3</td>
      <td>1</td>
      <td>2</td>
      <td>9</td>
    </tr>
    <tr class="even">
      <td>4</td>
      <td>2</td>
      <td>n/a</td>
      <td>3</td>
    </tr>
    <tr class="odd">
      <td>5</td>
      <td>2</td>
      <td>n/a</td>
      <td>9</td>
    </tr>
    <tr class="even">
      <td>6</td>
      <td>3</td>
      <td>n/a</td>
      <td>3</td>
    </tr>
    <tr class="odd">
      <td>7</td>
      <td>3</td>
      <td>n/a</td>
      <td>9</td>
    </tr>
    <tr class="even">
      <td>8</td>
      <td>4</td>
      <td>n/a</td>
      <td>3</td>
    </tr>
    <tr class="odd">
      <td>9</td>
      <td>4</td>
      <td>n/a</td>
      <td>9</td>
    </tr>
  </tbody>
</table>

Note that LCTs from chambers with CSCID=1 are transmitted in fragments over 8 links. Each of the 8 links carries a copy of BC0 signal from chamber CSCID=1. If any of these copies of BC0 signal are not received correctly, the corresponding flag (SXC1) in Table 5 is set to 1.

## Event Data

The Event Data consists of TBIN Data Blocks, if non-Zero Suppression mode is selected (SZ = 0):
The Event Data consists of 0 to TBIN Data Blocks with valid data, if Zero Suppression mode is selected (SZ = 1):

### Data Block

The Data Block content is determined by the Event Configuration Word (DD/CSR _DFC) and the proper data and consists of the following:

-   1 64-bit ME Data word per each CSC EMU ME LCT;
-   1 64-bit RPC Data word per each RPC partition
-   1 64-bit GEM Data word per each GEM cluster
-   2 64-bit SP Data words per SPz Track

#### ME Data Record

Each track segment is reported using one 64-bit word. Note that only valid segments are included into DAQ stream; invalid segments are skipped.

**Table 6a: MEx Data Record, legacy TMB data with repurposed bits, Frame 0**

<table style="width:100%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>D15</strong></th>
<th><strong>D14</strong></th>
<th><strong>D13</strong></th>
<th><strong>D12</strong></th>
<th><strong>D11</strong></th>
<th><strong>D10</strong></th>
<th><strong>D9</strong></th>
<th><strong>D8</strong></th>
<th><strong>D7</strong></th>
<th><strong>D6</strong></th>
<th><strong>D5</strong></th>
<th><strong>D4</strong></th>
<th><strong>D3</strong></th>
<th><strong>D2</strong></th>
<th><strong>D1</strong></th>
<th><strong>D0</strong></th>
<th><strong>Word</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1</td>
<td colspan="7">Key wire group [6:0]</td>
<td>0</td>
<td colspan="3">Quality [2:0]</td>
<td colspan="4">CLCT pattern [3:0]</td>
<td>MEa</td>
</tr>
<tr class="even">
<td>1</td>
<td>0</td>
<td>BXN0</td>
<td>L/R</td>
<td colspan="4">CSCID_true [3:0]</td>
<td colspan="8">CLCT key half-strip [7:0]</td>
<td>MEb</td>
</tr>
<tr class="odd">
<td>0</td>
<td>0</td>
<td>0</td>
<td>Frm=0</td>
<td colspan="12">0</td>
<td>MEc</td>
</tr>
<tr class="even">
<td>0</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td colspan="4">CSCID_TMB [3:0]</td>
<td>0</td>
<td colspan="3">Station [2:0]</td>
<td>VP</td>
<td colspan="3">TBIN Num [2:0]</td>
<td>MEd</td>
</tr>
</tbody>
</table>

**Table 6b: MEx Data Record, legacy TMB data with repurposed bits, Frame 1**

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>D15</strong></th>
<th><strong>D14</strong></th>
<th><strong>D13</strong></th>
<th><strong>D12</strong></th>
<th><strong>D11</strong></th>
<th><strong>D10</strong></th>
<th><strong>D9</strong></th>
<th><strong>D8</strong></th>
<th><strong>D7</strong></th>
<th><strong>D6</strong></th>
<th><strong>D5</strong></th>
<th><strong>D4</strong></th>
<th><strong>D3</strong></th>
<th><strong>D2</strong></th>
<th colspan="2"><strong>D1</strong></th>
<th><strong>D0</strong></th>
<th><strong>Word</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1</td>
<td colspan="7">Key wire group [6:0]</td>
<td>0</td>
<td colspan="3">Quality [3:0]</td>
<td colspan="3">HMT [3:1]</td>
<td colspan="2"><p>CPat</p>
<p>[4]</p></td>
<td>MEa</td>
</tr>
<tr class="even">
<td>1</td>
<td>0</td>
<td>HMT[0]</td>
<td>L/R</td>
<td colspan="4">CSCID_true [3:0]</td>
<td colspan="9">CLCT key half-strip [7:0]</td>
<td>MEb</td>
</tr>
<tr class="odd">
<td>0</td>
<td>0</td>
<td>0</td>
<td>Frm=1</td>
<td colspan="13">0</td>
<td>MEc</td>
</tr>
<tr class="even">
<td>0</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td colspan="4">CSCID_TMB [3:0]</td>
<td>0</td>
<td colspan="3">Station [2:0]</td>
<td>VP</td>
<td colspan="4">TBIN Num [2:0]</td>
<td>MEd</td>
</tr>
</tbody>
</table>

**Table 6c: MEx Data Record, OTMB with repurposed bits, Frame 0**

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>D15</strong></th>
<th><strong>D14</strong></th>
<th><strong>D13</strong></th>
<th><strong>D12</strong></th>
<th><strong>D11</strong></th>
<th><strong>D10</strong></th>
<th><strong>D9</strong></th>
<th><strong>D8</strong></th>
<th><strong>D7</strong></th>
<th><strong>D6</strong></th>
<th><strong>D5</strong></th>
<th><strong>D4</strong></th>
<th><strong>D3</strong></th>
<th><strong>D2</strong></th>
<th><strong>D1</strong></th>
<th><strong>D0</strong></th>
<th><strong>Word</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1</td>
<td colspan="7">Key wire group [6:0]</td>
<td>QS</td>
<td colspan="3">Quality [2:0]</td>
<td colspan="4">CLCT pattern [3:0]</td>
<td>MEa</td>
</tr>
<tr class="even">
<td>1</td>
<td>0</td>
<td>BXN0</td>
<td>L/R</td>
<td colspan="4">CSCID_true [3:0]</td>
<td colspan="8">CLCT key half-strip [7:0]</td>
<td>MEb</td>
</tr>
<tr class="odd">
<td>0</td>
<td>0</td>
<td>0</td>
<td>Frm=0</td>
<td colspan="12">0</td>
<td>MEc</td>
</tr>
<tr class="even">
<td>0</td>
<td>0</td>
<td>ES</td>
<td>0</td>
<td colspan="4">BEND [3:0]</td>
<td>0</td>
<td colspan="3">Station [2:0]</td>
<td>VP</td>
<td colspan="3">TBIN Num [2:0]</td>
<td>MEd</td>
</tr>
</tbody>
</table>

**Table 6d: MEx Data Record, OTMB with repurposed bits, Frame 1**

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>D15</strong></th>
<th><strong>D14</strong></th>
<th><strong>D13</strong></th>
<th><strong>D12</strong></th>
<th><strong>D11</strong></th>
<th><strong>D10</strong></th>
<th><strong>D9</strong></th>
<th><strong>D8</strong></th>
<th><strong>D7</strong></th>
<th><strong>D6</strong></th>
<th><strong>D5</strong></th>
<th><strong>D4</strong></th>
<th><strong>D3</strong></th>
<th><strong>D2</strong></th>
<th colspan="2"><strong>D1</strong></th>
<th><strong>D0</strong></th>
<th><strong>Word</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1</td>
<td colspan="7">Key wire group [6:0]</td>
<td>QS</td>
<td colspan="3">Quality [2:0]</td>
<td colspan="3">HMT [3:1]</td>
<td colspan="2"><p>CPat</p>
<p>[4]</p></td>
<td>MEa</td>
</tr>
<tr class="even">
<td>1</td>
<td>0</td>
<td>HMT[0]</td>
<td>L/R</td>
<td colspan="4">CSCID_true [3:0]</td>
<td colspan="9">CLCT key half-strip [7:0]</td>
<td>MEb</td>
</tr>
<tr class="odd">
<td>0</td>
<td>0</td>
<td>0</td>
<td>Frm=1</td>
<td colspan="13">0</td>
<td>MEc</td>
</tr>
<tr class="even">
<td>0</td>
<td>0</td>
<td>ES</td>
<td>0</td>
<td colspan="4">BEND [3:0]</td>
<td>HMV</td>
<td colspan="3">Station [2:0]</td>
<td>VP</td>
<td colspan="4">TBIN Num [2:0]</td>
<td>MEd</td>
</tr>
</tbody>
</table>

Note that the OTMB frame format depends on which frame number is it, 0 or 1. This is because the bits are repurposed differently in Frames 0 and 1 in TMB firmware. The “Frm” field indicates the frame number. The unpacking software should take Frm value into account when decoding each frame.

Here:

- CLCT Pattern (aka CPat) [4:0] or [3:0] – 5-bit or 4-bit CLCT pattern number
- Key Wire Group [6:0] – 7-bit value indicates the position of the pattern within the chamber and runs from 0 to 111;
- CLCT Key half-strip [7:0] – 8-bit value is between 0 and 159;
- ES = 1/8 strip, QS = ¼ strip
- CSCID_true [3:0] – 4-bit CSC ID indicates the chamber # and runs from 1 to 9;
- CSCID_TMB [3:0] – CSCID bits reported by TMB. These bits are repurposed in OTMB.
- Station: ME station number. 0 = ME1a, 1 = ME1b, 2 = ME2, 3 = ME3, 4 = ME4, 5 = Neighbor sector all stations, see table below.

<table style="width:100%;">
<colgroup>
<col style="width: 24%" />
<col style="width: 24%" />
<col style="width: 25%" />
<col style="width: 24%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>CSCID in station 5</strong></th>
<th><strong>Neighbor sector station</strong></th>
<th><strong>Neighbor sector subsector</strong></th>
<th><strong>Neighbor sector CSCID</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1</td>
<td>1</td>
<td>2</td>
<td>3</td>
</tr>
<tr class="even">
<td>2</td>
<td>1</td>
<td>2</td>
<td>6</td>
</tr>
<tr class="odd">
<td>3</td>
<td>1</td>
<td>2</td>
<td>9</td>
</tr>
<tr class="even">
<td>4</td>
<td>2</td>
<td>n/a</td>
<td>3</td>
</tr>
<tr class="odd">
<td>5</td>
<td>2</td>
<td>n/a</td>
<td>9</td>
</tr>
<tr class="even">
<td>6</td>
<td>3</td>
<td>n/a</td>
<td>3</td>
</tr>
<tr class="odd">
<td>7</td>
<td>3</td>
<td>n/a</td>
<td>9</td>
</tr>
<tr class="even">
<td>8</td>
<td>4</td>
<td>n/a</td>
<td>3</td>
</tr>
<tr class="odd">
<td>9</td>
<td>4</td>
<td>n/a</td>
<td>9</td>
</tr>
</tbody>
</table>

-   L/R – Left/Right bend bit indicates whether the track is heading towards lower or higher strip number;
-   VP – LCT Valid bit.
-   TBIN Num [2:0] – data block time bin number within the DAQ readout window.
-   HMT[3:0] = High-Multiplicity Trigger bits. Note that the ME data record can be valid when HMT field is non-zero, but LCT may be invalid.
-   HMV = High-Multiplicity Valid flag
-   FRM = MPC frame number, 0 or 1. HMV should only be set when FRM == 1

#### RPC Data Record

Each RPC primitive is reported using one 64-bit word. Note that only valid primitives are included into DAQ stream; invalid primitives are skipped.

**Table 7: RPC Data Record**

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 6%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>D15</strong></th>
<th><strong>D14</strong></th>
<th><strong>D13</strong></th>
<th><strong>D12</strong></th>
<th><strong>D11</strong></th>
<th><strong>D10</strong></th>
<th><strong>D9</strong></th>
<th><strong>D8</strong></th>
<th><strong>D7</strong></th>
<th><strong>D6</strong></th>
<th><strong>D5</strong></th>
<th><strong>D4</strong></th>
<th><strong>D3</strong></th>
<th><strong>D2</strong></th>
<th><strong>D1</strong></th>
<th><strong>D0</strong></th>
<th><strong>Word</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>0</td>
<td colspan="2">Frame[1:0]</td>
<td colspan="2">Word[1:0]</td>
<td colspan="11">Phi [10:0]</td>
<td>RPCa</td>
</tr>
<tr class="even">
<td>1</td>
<td colspan="3">Link[2:0]</td>
<td>0</td>
<td>0</td>
<td>BC0</td>
<td colspan="5">Theta [4:0]</td>
<td>VP</td>
<td colspan="3">TBIN Num [2:0]</td>
<td>RPCb</td>
</tr>
<tr class="odd">
<td>0</td>
<td colspan="2">Frame[1:0]</td>
<td colspan="2">Word[1:0]</td>
<td colspan="11">Phi [10:0]</td>
<td>RPCc</td>
</tr>
<tr class="even">
<td>0</td>
<td colspan="3">Link[2:0]</td>
<td>0</td>
<td>0</td>
<td>BC0</td>
<td colspan="5">Theta [4:0]</td>
<td>VP</td>
<td colspan="3">TBIN Num [2:0]</td>
<td>RPCd</td>
</tr>
</tbody>
</table>

Each RPC record contains up to two RPC hits coming from the same RPC chamber. Words RPCa and RPCb contain parameters for Hit 0, words RPCc and RPCd for Hit 1.

Here:

-   Phi – phi coordinate reported by CPPF
-   Theta – theta coordinate reported by CPPF
-   Link – 10G link number (0..6)

<table>
<colgroup>
<col style="width: 18%" />
<col style="width: 25%" />
<col style="width: 55%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Link number</strong></th>
<th><strong>Link index in DAQ</strong></th>
<th><strong>10° RPC Subsector within 60° sector</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1</td>
<td>0</td>
<td>0°-10°</td>
</tr>
<tr class="even">
<td>2</td>
<td>1</td>
<td>10°-20°</td>
</tr>
<tr class="odd">
<td>3</td>
<td>2</td>
<td>20°-30°</td>
</tr>
<tr class="even">
<td>4</td>
<td>3</td>
<td>30°-40°</td>
</tr>
<tr class="odd">
<td>5</td>
<td>4</td>
<td>40°-50°</td>
</tr>
<tr class="even">
<td>6</td>
<td>5</td>
<td>50°-60°</td>
</tr>
<tr class="odd">
<td>7</td>
<td>6</td>
<td>50°-60° from neighbor sector</td>
</tr>
</tbody>
</table>

- Frame – 64-bit frame number within link (0..2)
- Word – 16-bit word number within frame (0..3)

<table>
<colgroup>
<col style="width: 23%" />
<col style="width: 22%" />
<col style="width: 27%" />
<col style="width: 26%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>RPC chamber</strong></th>
<th><strong>Segment</strong></th>
<th><strong>Frame index in DAQ</strong></th>
<th><strong>Word index in DAQ</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>RE1/2</td>
<td>0</td>
<td>0</td>
<td>0</td>
</tr>
<tr class="even">
<td>RE1/2</td>
<td>1</td>
<td>0</td>
<td>1</td>
</tr>
<tr class="odd">
<td>RE2/2</td>
<td>0</td>
<td>0</td>
<td>2</td>
</tr>
<tr class="even">
<td>RE2/2</td>
<td>1</td>
<td>0</td>
<td>3</td>
</tr>
<tr class="odd">
<td>RE3/2</td>
<td>0</td>
<td>1</td>
<td>0</td>
</tr>
<tr class="even">
<td>RE3/2</td>
<td>1</td>
<td>1</td>
<td>1</td>
</tr>
<tr class="odd">
<td>RE3/3</td>
<td>0</td>
<td>1</td>
<td>2</td>
</tr>
<tr class="even">
<td>RE3/3</td>
<td>1</td>
<td>1</td>
<td>3</td>
</tr>
<tr class="odd">
<td>RE4/2</td>
<td>0</td>
<td>2</td>
<td>0</td>
</tr>
<tr class="even">
<td>RE4/2</td>
<td>1</td>
<td>2</td>
<td>1</td>
</tr>
<tr class="odd">
<td>RE4/3</td>
<td>0</td>
<td>2</td>
<td>2</td>
</tr>
<tr class="even">
<td>RE4/3</td>
<td>1</td>
<td>2</td>
<td>3</td>
</tr>
</tbody>
</table>

- TBIN Num – data block time bin number within the DAQ readout window.
- VP – “valid pattern” bit
- BC0 is the BC0 bit this chamber
    - Currently not implemented, always 0

#### GE1/1, GE2/1 Data Record

Each GEM cluster is reported using one 64-bit word. Note that only valid clusters are included into DAQ stream; invalid clusters are skipped.

**Table 8: GEM record data format**

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>D15</strong></th>
<th><strong>D14</strong></th>
<th><strong>D13</strong></th>
<th><strong>D12</strong></th>
<th><strong>D11</strong></th>
<th><strong>D10</strong></th>
<th><strong>D9</strong></th>
<th><strong>D8</strong></th>
<th><strong>D7</strong></th>
<th><strong>D6</strong></th>
<th><strong>D5</strong></th>
<th><strong>D4</strong></th>
<th><strong>D3</strong></th>
<th><strong>D2</strong></th>
<th><strong>D1</strong></th>
<th><strong>D0</strong></th>
<th><strong>Word</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1</td>
<td colspan="3">Cluster size[2:0]</td>
<td colspan="3">Partition[2:0]</td>
<td colspan="9">Strip[8:0]</td>
<td>G11a</td>
</tr>
<tr class="even">
<td>1</td>
<td colspan="3">Link[2:0]</td>
<td colspan="4">Cluster # [3:0]</td>
<td>BC0</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td>VP</td>
<td colspan="3">TBIN Num [2:0]</td>
<td>G11b</td>
</tr>
<tr class="odd">
<td>1</td>
<td colspan="3">Cluster size[2:0]</td>
<td colspan="3">Partition[2:0]</td>
<td colspan="9">Strip[8:0]</td>
<td>G11c</td>
</tr>
<tr class="even">
<td>0</td>
<td colspan="3">Link[2:0]</td>
<td colspan="4">Cluster # [3:0]</td>
<td>BC0</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td>VP</td>
<td colspan="3">TBIN Num [2:0]</td>
<td>G11d</td>
</tr>
</tbody>
</table>

Each GEM record contains up to two GEM clusters from the same superchamber. Words GEMa and GEMb contain parameters for cluster from Layer 0, words GEMc and GEMd for cluster from Layer 1.

Here:

-   Strip is the strip number reported by GEM chamber
-   Partition is the partition number reported by GEM chamber
-   Cluster size is the size of the cluster reported by GEM chamber
-   Link is the optical link number the data came from

<table>
<colgroup>
<col style="width: 18%" />
<col style="width: 25%" />
<col style="width: 55%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Link number</strong></th>
<th><strong>Link index in DAQ</strong></th>
<th><strong>10° GEM Subsector within 60° sector</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1</td>
<td>0</td>
<td>0°-10°</td>
</tr>
<tr class="even">
<td>2</td>
<td>1</td>
<td>10°-20°</td>
</tr>
<tr class="odd">
<td>3</td>
<td>2</td>
<td>20°-30°</td>
</tr>
<tr class="even">
<td>4</td>
<td>3</td>
<td>30°-40°</td>
</tr>
<tr class="odd">
<td>5</td>
<td>4</td>
<td>40°-50°</td>
</tr>
<tr class="even">
<td>6</td>
<td>5</td>
<td>50°-60°</td>
</tr>
<tr class="odd">
<td>7</td>
<td>6</td>
<td>50°-60° from neighbor sector</td>
</tr>
</tbody>
</table>

- Cluster \# is the number of the cluster within data frame that came from link (0..16)
- TBIN Num – data block time bin number within the DAQ readout window.
- VP – “valid pattern” bit
- BC0 is the BC0 bit from layer 0/1 in this chamber
    - Currently not implemented, always 0

#### SP Output Data Record

Each output track is reported using two 64-bit words. Note that only valid tracks are included into DAQ stream; invalid tracks are skipped.

**Table 9: SPz Output Data Record**

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 0%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 0%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 7%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>D15</strong></th>
<th><strong>D14</strong></th>
<th><strong>D13</strong></th>
<th><strong>D12</strong></th>
<th><strong>D11</strong></th>
<th><strong>D10</strong></th>
<th colspan="2"><strong>D9</strong></th>
<th><strong>D8</strong></th>
<th><strong>D7</strong></th>
<th><strong>D6</strong></th>
<th colspan="2"><strong>D5</strong></th>
<th><strong>D4</strong></th>
<th><strong>D3</strong></th>
<th><strong>D2</strong></th>
<th><strong>D1</strong></th>
<th><strong>D0</strong></th>
<th><strong>Word</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1</td>
<td>HL</td>
<td>C</td>
<td colspan="15">Phi_full [12:0]</td>
<td>SP1a</td>
</tr>
<tr class="even">
<td>0</td>
<td>VC</td>
<td>SE</td>
<td>BC0</td>
<td colspan="5">Quality_GMT[3:0]</td>
<td colspan="9">Phi_GMT[7:0]</td>
<td>SP1b</td>
</tr>
<tr class="odd">
<td>1</td>
<td colspan="2">HMT[1:0]</td>
<td colspan="5">Quality [3:0]</td>
<td colspan="10">Eta_GMT [8:0]</td>
<td>SP1c</td>
</tr>
<tr class="even">
<td>0</td>
<td colspan="7">ME1_ID[5:0]</td>
<td colspan="10">Pt [8:0]</td>
<td>SP1d</td>
</tr>
<tr class="odd">
<td>0</td>
<td colspan="6">ME4_ID[4:0]</td>
<td colspan="6">ME3_ID[4:0]</td>
<td colspan="5">ME2_ID[4:0]</td>
<td>SP2a</td>
</tr>
<tr class="even">
<td>1</td>
<td colspan="3">TBIN Num[2:0]</td>
<td colspan="4">ME4_delay[2:0]</td>
<td colspan="4">ME3_delay[2:0]</td>
<td colspan="3">ME2_delay[2:0]</td>
<td colspan="3">ME1_delay[2:0]</td>
<td>SP2b</td>
</tr>
<tr class="odd">
<td>1</td>
<td colspan="17">PT_LUT_address or NN_PT [14:0]</td>
<td>SP2c</td>
</tr>
<tr class="even">
<td>0</td>
<td colspan="17">PT_LUT_address or NN_PT [29:15]</td>
<td>SP2d</td>
</tr>
</tbody>
</table>

Here:

- Phi_full is the track’s phi coordinate as measured on the innermost available station, full precision
- Phi_GMT is the track’s phi coordinate as reported to GMT
- Eta_GMT is the track’s eta coordinate as reported to GMT
- Pt is the track’s transverse momentum (output of PT LUT)
- Quality is the track’s quality code. Bit assignment is shown below:  
    - bit 0 = ME4 hits present  
    - bit 1 = ME3 hits present  
    - bit 2 = ME2 hits present  
    - bit 3 = ME1 hits present
- Quality_GMT is the quality code as reported to GMT
- “PT_LUT_address or NN_PT” field changes meaning depending on “use_nn_pt” configuration register.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 79%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>use_nn_pt flag</strong></th>
<th><strong>Field value</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>0</td>
<td>PT LUT Address applied to PT LUT memory inputs</td>
</tr>
<tr class="even">
<td>1</td>
<td><p>Neural Net PT output</p>
<table>
<colgroup>
<col style="width: 18%" />
<col style="width: 81%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Bits</strong></th>
<th><strong>Value</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>7:0</td>
<td>NN_PT value</td>
</tr>
<tr class="even">
<td>10:8</td>
<td>NN_D0 value</td>
</tr>
<tr class="odd">
<td>11</td>
<td>MN_PT valid flag</td>
</tr>
<tr class="even">
<td>29:12</td>
<td>Filled with zeros</td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

-   is the address of the PT LUT memory that was formed by track-finder logic
-   C is the track’s charge
-   VC if set to 1 indicates that the track data OR HMT bits are valid.
-   BX are two least significant bits of BX assigned to this track
-   MEx_ID fields show which track stubs (from corresponding MEx stations) were used to build this track. Value of 0 in these fields means that the track stub from that station was not used. Format of these fields is shown below:

**ME1_ID format:**

<table>
<colgroup>
<col style="width: 37%" />
<col style="width: 21%" />
<col style="width: 41%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>D5</strong></th>
<th><strong>D4..D1</strong></th>
<th><strong>D0</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>0 = Subsector 1</p>
<p>1 = Subsector 2</p></td>
<td>CSC ID</td>
<td>Track stub number</td>
</tr>
</tbody>
</table>

**ME2,3,4_ID format:**

<table>
<colgroup>
<col style="width: 34%" />
<col style="width: 65%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>D4..D1</strong></th>
<th><strong>D0</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>CSC ID</td>
<td>Track stub number</td>
</tr>
</tbody>
</table>

Each chamber may send up to two track stubs per BX. “Track stub number” fields indicate which track stub was used (0 = first, 1 = second). CSC ID set to 0 indicates that no valid track stub from this station was used to build the track.

MEx_ID CSC ID fields can take the following values:

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 32%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>ME1_ID CSC ID value</strong></th>
<th><strong>Sector</strong></th>
<th><strong>CSCID</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>0</td>
<td>N/A</td>
<td>No valid LCT</td>
</tr>
<tr class="even">
<td>1 .. 9</td>
<td>Native sector for this EMTF</td>
<td>1 .. 9</td>
</tr>
<tr class="odd">
<td>10</td>
<td>Neighbor, subsector 2</td>
<td>3</td>
</tr>
<tr class="even">
<td>11</td>
<td>Neighbor, subsector 2</td>
<td>6</td>
</tr>
<tr class="odd">
<td>12</td>
<td>Neighbor, subsector 2</td>
<td>9</td>
</tr>
</tbody>
</table>

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 32%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>ME2,3,4_ID CSC ID value</strong></th>
<th><strong>Sector</strong></th>
<th><strong>CSCID</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>0</td>
<td>N/A</td>
<td>No valid LCT</td>
</tr>
<tr class="even">
<td>1 .. 9</td>
<td>Native sector for this EMTF</td>
<td>1 .. 9</td>
</tr>
<tr class="odd">
<td>10</td>
<td>Neighbor</td>
<td>3</td>
</tr>
<tr class="even">
<td>11</td>
<td>Neighbor</td>
<td>6</td>
</tr>
</tbody>
</table>

- ME1_delay, ME2_delay, ME3_delay, ME4_delay are delays applied to the track stubs to build this track. Possible values are 0,1,2. If all stubs came at the same BX, delays for them will be all =2. For stubs that came late by one BX, the delay will be =1, and for stubs that were late by 2 BX the delay will be set to 0.
- BXN0 – reserved
- BC0 – reserved
- TBIN Num – data block time bin number within the DAQ readout window.
- HMT – high-multiplicity trigger bits

Note that RPC track stub IDs are not implemented at this time. They will be implemented in future versions.

### Event Record Trailer

The Event Record Trailer consists of two 64-bit words, where the most significant hex digit in each 16-bit word is the legacy DDU Code word 0xF and 0xE.

**Table 10: Event Record Trailer**

<table>
<colgroup>
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 6%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 6%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>D15</strong></th>
<th><strong>D14</strong></th>
<th><strong>D13</strong></th>
<th><strong>D12</strong></th>
<th><strong>D11</strong></th>
<th><strong>D10</strong></th>
<th><strong>D9</strong></th>
<th><strong>D8</strong></th>
<th><strong>D7</strong></th>
<th><strong>D6</strong></th>
<th><strong>D5</strong></th>
<th><strong>D4</strong></th>
<th>D3</th>
<th><strong>D2</strong></th>
<th><strong>D1</strong></th>
<th><strong>D0</strong></th>
<th>Word</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td colspan="4">0xF</td>
<td colspan="4">DD/CSR LF[3:0]</td>
<td colspan="8">L1A[7:0]</td>
<td>TR1a</td>
</tr>
<tr class="even">
<td colspan="4">0xF</td>
<td colspan="4">DD/CSR LF[7:4]</td>
<td>LFFF</td>
<td colspan="3">0x7</td>
<td colspan="4">0xF</td>
<td>TR1b</td>
</tr>
<tr class="odd">
<td colspan="4">0xF</td>
<td>0</td>
<td>0</td>
<td colspan="6">YY[5:0]</td>
<td colspan="4">MM[3:0]</td>
<td>TR1c</td>
</tr>
<tr class="even">
<td colspan="4">0xF</td>
<td colspan="12">0xabc</td>
<td>TR1d</td>
</tr>
<tr class="odd">
<td colspan="4">0xE</td>
<td>0</td>
<td>0</td>
<td colspan="5">HH[4:0]</td>
<td colspan="5">DD[4:0]</td>
<td>TR2a</td>
</tr>
<tr class="even">
<td colspan="4">0xE</td>
<td colspan="6">SEC[5:0]</td>
<td colspan="6">MIN[5:0]</td>
<td>TR2b</td>
</tr>
<tr class="odd">
<td colspan="4">0xE</td>
<td colspan="12">0</td>
<td>TR2c</td>
</tr>
<tr class="even">
<td colspan="4">0xE</td>
<td colspan="12">0</td>
<td>TR2d</td>
</tr>
</tbody>
</table>

Here:

- L1A [7:0] – Event Number, lower byte, same as HD1a [7:0];
- DD/CSR_LF [7:0] = 0…255 – L1A FIFO word count. Shows the L1A queue size at the moment of transmitting TR1a;
- LFFF = DD/CSR_LF[15] – L1A FIFO Full Flag (LF word count = 256) at the moment of transmitting TR1a;
- Firmware time stamp fields:
    - YY – year
    - MM – month
    - DD – day
    - HH – hour
    - MIN – minute
    - SEC - second

## **DAQ unit logic structure**

**Figure *1*** shows the major building blocks of the DAQ module. Trigger primitives from detectors (CSC and RPC) are first fed into delay lines. Each delay line is adjusted to delay for precisely the latency of L1A, taking the latency of data arrival into account. In particular, RPC data arrive later than CSC, so the RPC delay line latency is made shorter to compensate for that. These latencies are adjustable via PCIe control interface.

Each L1A signal arrives at the time when trigger primitives corresponding to it are exiting the delay lines. The L1A signal serves as a “Write enable” input for Ring buffer, that stores the relevant trigger primitives for reporting to DAQ system.

Finally, the Formatter state machine is responsible for actually forming the DAQ data packets according to this specification, and sending them to AMC13 board, which in its turn funnels the data to Central CMS DAQ system. The AMC13 board reports back its status; the Formatter is only allowed to send data when AMC13 is ready to receive it.

**Figure 1: DAQ module building blocks**

![](assets/daq/module_bb.png)

## Revision history

<table>
<colgroup>
<col style="width: 17%" />
<col style="width: 82%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Date</strong></th>
<th><strong>Notes</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>2014-07-15</td>
<td><p>Converted from Lev Uvarov’s specification of the legacy DAQ format:</p>
<p><a href="http://www.phys.ufl.edu/~uvarov/SP05/LU-SP2DDU_Event_Record_Structure_v53.pdf">http://www.phys.ufl.edu/~uvarov/SP05/LU-SP2DDU_Event_Record_Structure_v53.pdf</a></p>
<p>Significantly reworked to adapt the format for MTF7. This is an initial draft; it will be modified in the future.</p></td>
</tr>
<tr class="even">
<td>2014-07-21</td>
<td>SP output data record reworked to allocate more bits for track segment IDs. Each track stub ID is now a 5-bit value.</td>
</tr>
<tr class="odd">
<td>2015-03-18</td>
<td>Removed RPC enable flags from header for now. Made ME enable flags longer to accommodate chambers from neighbor sector.</td>
</tr>
<tr class="even">
<td>2015-10-09</td>
<td><p>Increased ME1_ID field length to 6 bits, to accommodate both subsectors + track stub number</p>
<p>Added VT bit (valid track)</p></td>
</tr>
<tr class="odd">
<td>2015-12-03</td>
<td>Fixed errors in Table 8, added GMT track parameters to output<br>
Removed FMM section copied from DAQ document</td>
</tr>
<tr class="even">
<td>2016-03-15</td>
<td>Added better explanation to MEx_ID and Quality fields in SP Output data record</td>
</tr>
<tr class="odd">
<td>2016-03-30</td>
<td><p>SP output record rework, to accommodate neighbor sector’s chambers and GMT quality output:</p>
<ul>
<li><p>Added Quality_GMT field</p></li>
<li><p>Removed VT bit – if the record is present, the track is valid.</p></li>
<li><p>Expanded phi_full field to 13 bits</p></li>
<li><p>Moved VC bit</p></li>
</ul></td>
</tr>
<tr class="even">
<td>2016-04-08</td>
<td><ul>
<li><p>In ME record, station 5 now represents all sectors from neighbor sector</p></li>
</ul>
<ul>
<li><p>In SP output record, extra CSCID values are used to mark LCTs from neighbor sector</p></li>
<li><p>In SP output record, renamed MEx_TBIN fields into MEx_delay. Added better explanation for them.</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>2016-10-12</td>
<td>Replaced Track and Orbit counter section with MPC link error section.</td>
</tr>
<tr class="even">
<td>2017-02-03</td>
<td>Reworked RPC data format to accommodate data received from CPPF boards</td>
</tr>
<tr class="odd">
<td>2017-03-17</td>
<td>Added RPC link-to-subsector assignment and frame/word to chamber/segment assignment.</td>
</tr>
<tr class="even">
<td>2018-03-23</td>
<td>Reworked trailer format to reflect what’s actually implemented in firmware</td>
</tr>
<tr class="odd">
<td>2018-05-01</td>
<td>Added CPPF enable flags and CPPF CRC match flags to header</td>
</tr>
<tr class="even">
<td>2020-05-21</td>
<td>Added GE1/1 data</td>
</tr>
<tr class="odd">
<td>2021-11-24</td>
<td>Added Neural Net PT output</td>
</tr>
<tr class="even">
<td>2021-11-30</td>
<td><ul>
<li><p>Fixed error in NN PT output bit numbers</p></li>
<li><p>Updated Figure 1</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>2022-01-09</td>
<td><ul>
<li><p>In SP output section: Replaced BX[1:0] bits with HMT bits in DAQ output</p></li>
<li><p>In DAQ header section: added use_nn_pt configuration bit, to make decoding easier</p></li>
</ul></td>
</tr>
<tr class="even">
<td>2022-03-16</td>
<td><ul>
<li><p>Adapted DAQ module to upgraded GE1/1 data format</p></li>
<li><p>Added BC0 from superchamber layer 1 to GEM section</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>2022-05-16</td>
<td><p>ME DAQ record rework:</p>
<ul>
<li><p>Removed unimplemented fields, replaced with zeros</p></li>
<li><p>Added HMT trigger and HMT valid fields</p></li>
<li><p>Reworked field descriptions</p></li>
</ul></td>
</tr>
<tr class="even">
<td>2022-05-30</td>
<td>ME DAQ record rework:<br>
replaced Quality field with ES and QS bits. Quality is unused by EMTF logic anyway.</td>
</tr>
<tr class="odd">
<td>2022-07-04</td>
<td><p>Fixed errors in legacy TMB DAQ section:</p>
<ul>
<li><p>Added missing HMT bits</p></li>
<li><p>changed Quality code to 3 bits</p></li>
<li><p>zeroed out unused QS and ES bits</p></li>
</ul></td>
</tr>
<tr class="even">
<td>2022-08-02</td>
<td>Updated Table 2, max DAQ block sizes</td>
</tr>
<tr class="odd">
<td>2022-08-25</td>
<td>Reworked RPC and GEM section formats to transmit two hits or clusters per 64-bit word.</td>
</tr>
</tbody>
</table>

