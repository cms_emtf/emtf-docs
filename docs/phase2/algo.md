# Phase 2 EMTF Algorithm

## Overview

The EMTF algorithm can be broken into 4 main blocks: primitive conversion, pattern recognition, track building, and parameter assignment. Primitive Conversion involves converting trigger primitives recieved from muon chambers into emtf segments use interally by the algorithm. Those converted hits are used to build 3D "hitmaps", which are compared against muon track patterns for different $`p_T`$ and $`\eta`$ values. From these pattern matches or "roads" we select the best tracks and attach the emtf segments to each track to build a full track. This information is then sent to a Nueral Network to calculate a $`p_T`$ value (and a displacement value for displaced muons). Each sector processor runs this algorithm independently use trigger primitives from chambers within that sector. 


![](assets/algorithm_blocks.svg)


## Primitive Conversion

The most important function here is to convert trigger primitive into emtf segments used internally by the EMTF algorithm. In general, this means going from strips (in phi) and wires or pads (in eta) and converting them into integer $`\phi`$ and $`\theta`$ units. The integer $`\phi`$ unit is a local coordinate defined within a sector. 

The integer $`\phi`$ is $`1/60 = 0.0167^\circ`$, internally called "1/8-strip" (corresponding to an MEx/2 1/8th strip). It is encoded as a 13-bit integer (0 to 8191). The position 0 corresponds to $`-22^\circ`$ from the lower boundary of the sector. To convert from the integer $`\phi`$ unit to CMS global coordinate (in degrees):

```math
\phi_{\text{sector}} = \phi_{\text{integer}} / 60 − 22 \\
\phi_{\text{CMS}} = \phi_{\text{sector}} + 15 + (\text{sector} − 1) \times 60 \\
\text{sector} \in {1,2,3,4,5,6} \\
```

The integer $`\theta`$ unit has a scale of approx $`0.285^\circ`$,
encoded as a 7-bit integer (0 to 127). The position 0 corresponds to $`8.5^\circ`$.
To convert from the integer $`\theta`$ unit to CMS global coordinate (in degrees):

```math
\theta_{\text{CMS}} = \theta_{\text{integer}} \cdot \left( \frac{45 - 8.5}{128} \right) + 8.5
```

In phase 2, the hardware will use pre-generated tables to look up these conversions directly. This will allow for better phi accuracy across all strips in a chamber, with the option to also use some eta information to better account for station displacement. The current firmware implementation use 3 additional partition bits for the ME0 phi lookup, 2 wiregroup or partition bits for ring 1 phi lookups, and 1 wiregroup bit for ring 2 phi lookups. Note: RPC's and iRPC's come to EMTF pre-converted into sector $`\phi`$ and $`\theta`$ units.


## Pattern Recognition

The trajectory of a muon as it passes through the endcap muon stations is bent by the magnetic field. Muons with low pT are bent more compared to muons with high pT. But as the magnetic field diminishes at large z, there is very little bending of the muon trajectories in the outer stations even for low pT muons. EMTF uses patterns of different straightness to detect muons by matching the hits to the patterns.

###Zones
For track finding, each sector is divided into 3(?) "zones" (from 0 to 2) along the θ direction, and pattern matching is run in each zone in parallel. The approximate boundaries of the 3 zones are shown below.

![](assets/approx_zones.png)

###Hitmaps

For each zone, we first create a hitmap using the emtf segments, this is just a 2d image of hits that occured in given a sector and zone. This hitmap uses a 'double-strip' resolution (1/8th strip * 16). Each sector has a full coverage of $`-22^\circ`$ to $`62^\circ`$ or phi values ranging from 0 to 5039. Converting this to double-strips gives a valid range of 0-314. The first 27 double-strips are not used for pattern matching and are removed, giving a full hitmap of 288 double-strip columns, representing hits ranging from  $`-14.8^\circ`$ to $`62^\circ`$ in phi. Each hitmap contains 8 rows representing different layers of chambers that a muon in a zone could have gone through. Some layers of chambers are merged into 1 row to keep all rows at 8 for every zone. 

![](assets/hitmaps.png)

### Patterns

Patterns are then compared agaist each hitmap to look for potential muon tracks for every column in the hitmap. For 7 patterns and 288 columns, this gives us 2016 pattern vs. hitmap comparisons per zone, each outputting an 8-bit 'activation' code. This code tells us whether a pattern matched a hit in a row when the pattern was placed at a certain column. The image below shows an activation code for 1 pattern at a single column.

![](assets/pattern_matching.png)

Using this activation code as an address, we look up the quality code associated with that activation. Currently each zone has it's own table, eventually each zone and pattern will likely have it's own activation table. The output of the table is a 6-bit quality code that tells us how well a track matched a pattern for a given key-double-strip (or key-column). These quality codes are then used to sort down to the best 4 potential tracks. The 21 patterns currently used are shown below.


![](assets/patterns.png)


## Track Building

Track Building involves taking the best tracks found during the pattern matching and attaching segments to that track. This is done by using the pattern and column for a given track to give us the phi range where a hit could be. When a hit falls within the pattern, we attach that hit to the track, when multiple hits match the pattern, we attach the one that is closest to the center of the pattern for a given row. Using these attached segments, we calculate a theta 'median' (not the actual median), and remove any hits that fall too far away from the median (8 integer values is the current limit). These attached segments are then converted into a set of features that can be input into the Neural Network for parameter assignment.

## Parameter Assignment

Parameter Assignment involves taking built tracks and assigning $`p_T`$ and displacement values. This is done using a sparsely connected neural network. The current prompt muon Nueral Network uses 3 dense layers.

