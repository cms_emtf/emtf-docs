
## EMTF Meeting: August 11, 2022

### Different number of segments

Patrick mentioned using different number of segments depending on the chamber type.

|Chamber Type       |Number of Segments |Notes                                          |
|-------------------|-------------------|-----------------------------------------------|
|CSC                |2                  |                                               |
|RPC Stations 1&2   |2                  |                                               |
|RPC Stations 3&4   |4                  |These two stations are treated as one chamber  |
|GEM                |4                  |Takes in 8 per superchamber                    |
|iRPC               |4                  |                                               |
|ME0                |8                  |To be decided                                  |

### Calculating column start

Jia Fu's Code contains:

```cpp
// ph_init reference values
// [0, 38, 75, 113, 150, 188, 225, 263] -> [0.0, 10.1333, 20.0, 30.1333, 40.0, 50.1333, 60.0, 70.1333] deg
constexpr static const int chamber_ph_init_10deg[7]       = { 75, 113, 150, 188, 225, 263,  38};
constexpr static const int chamber_ph_init_20deg[4]       = { 75, 150, 225,   0};
constexpr static const int chamber_ph_init_20deg_ext[11]  = { 75, 150, 225,   0,  75, 113, 150, 188, 225, 263,  38};

// ph_cover reference values
// [52, 90, 127, 165, 202, 240, 277, 315] -> [13.8667, 24.0, 33.8667, 44.0, 53.8667, 64.0, 73.8667, 84.0] deg
constexpr static const int chamber_ph_cover_10deg[7]      = {127, 165, 202, 240, 277, 315,  90};
constexpr static const int chamber_ph_cover_20deg[4]      = {165, 240, 315,  90};
constexpr static const int chamber_ph_cover_20deg_ext[11] = {165, 240, 315,  90, 127, 165, 202, 240, 277, 315,  90};
```

Patrick gave me a fomula to go from the column integer to degrees:
```math
\text{Degrees} = {84^\circ \over 315\ \text{Columns}} * Column
```
