# Performance

- Author: Osvaldo Miguel Colin
- Last Updated: 02/April/2023

## Parameter Assignment

Samples

- Prompt Single Muon Gun Positive Endcap - 1M events 
- Prompt Single Muon Gun Negative Endcap - 1M events 

Results

<img src="assets/vld_qpt.png" width=50%/>

<table>
  <tr>
    <th>$`q/p_{T}`$</th>
    <th>$`q`$</th>
  </tr>
  <tr>
    <td><img src="assets/vld_qinvpt.png"/></td>
    <td><img src="assets/vld_q.png"/></td>
  </tr>
</table>

## Efficiencies

Samples

- Prompt Single Muon Gun Positive Endcap - 1M events 
- Prompt Single Muon Gun Negative Endcap - 1M events 

Numerator for $`p_{T}`$

- trk_mode >= 12
- trk_qual >= 48
- trk_pt >= Threshold

Denominator for $`p_{T}`$

- gen_bx = 0
- 1.24 <= abs(gen_eta) <= 2.4

Results

<table>
  <tr>
    <th>$`p_{T}`$</th>
    <th>$`\eta`$</th>
  </tr>
  <tr>
    <td><img src="assets/emu_eff_vs_pt.png"/></td>    
    <td><img src="assets/emu_eff_vs_eta.png"/></td>
  </tr>
</table>

<table>
  <tr>
    <th>$`p_{T} \ge 10`$</th>
    <th>$`p_{T} \ge 20`$</th>
  </tr>
  <tr>
    <td><img src="assets/emu_eff_vs_pt_pt_ge_10GeV.png"/></td>
    <td><img src="assets/emu_eff_vs_pt_pt_ge_20GeV.png"/></td>
  </tr>
</table>

## Rates

Samples

- Single Neutrino Gun @ 200 PU - 1M events 
    - [Link](https://cmsweb.cern.ch/das/request?input=dataset%3D%2FMinBias_TuneCP5_14TeV-pythia8%2FPhase2HLTTDRSummer20ReRECOMiniAOD-PU200_111X_mcRun4_realistic_T15_v1-v1%2FFEVT&instance=prod/global) CMS DAS

Parameters Used
- orbit_freq = 11.246 kHz
- n_coll_bunches = 2760
    - assume lumi=8e34, PU=200, xsec_pp=80mb

Trigger Definitions

- $`p_{T}`$ Threshold
    - trk_qual >= 48
    - trk_mode >= 12
    - 1.2 <= abs(trk_eta) <= 2.4
    - trk_pt >= Threshold
    
- Quality Threshold
    - trk_qual >= Threshold
    - trk_mode >= 12
    - 1.2 <= abs(trk_eta) <= 2.4
    - trk_pt >= 20 GeV
    
Results

<table>
  <tr>
    <th>$`p_{T}`$</th>
    <th>Quality</th>
  </tr>
  <tr>
    <td><img src="assets/rate_vs_pt.png"/></td>
    <td><img src="assets/rate_vs_qual.png"/></td>
  </tr>
</table>

<table>
  <tr>
    <th>Zone 0</th>
    <th>Zone 1</th>
    <th>Zone 2</th>
  </tr>
  <tr>
    <td><img src="assets/rate_vs_pt_3.png"/></td>
    <td><img src="assets/rate_vs_pt_2.png"/></td>
    <td><img src="assets/rate_vs_pt_1.png"/></td>
  </tr>
</table>

