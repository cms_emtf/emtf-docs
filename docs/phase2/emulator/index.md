# EMTF++ Emulator

- Author: Osvaldo Miguel Colin
- Last Updated: 17/March/2023

## Orginal Author

- Jia Fu Low (jiafu.low@gmail.com)

## Maintainers

- Osvaldo Miguel Colin (om15@rice.edu)

## Expert

- Patrick Kelling (pdk2@rice.edu)

## Status

- Waiting for approval from EMTF.
- Waiting for approval from Phase-2.

## Repositories

The emulator consists of two main packages:

- [Repository](https://gitlab.com/rice-acosta-group/emtfpp/l1triggerl1muonendcapphase2) Emulator
    - Contains code that emulates phase-2 algorithm.
- [Repository](https://gitlab.com/rice-acosta-group/emtfpp/dataformatsl1tmuonphase2) Data Formats
    - Contains data structures for EMTFHits, EMTFTracks, and EMTFInputs.

Several other packages are required for extended functionality. 
These packages are part of the EMTFTools package.

- [Repository](https://gitlab.com/rice-acosta-group/emtfpp/l1tmuonendcapntuplephase2) Phase-2 Validation
    - Compares outputs of events processed by FW with the outputs of the emulator.
- [Repository](https://gitlab.com/rice-acosta-group/emtfpp/l1tmuonendcapntuplephase2) Ntuple Maker
    - Data structures for EMTFHits, EMTFTracks, and EMTFInputs.
- [Repository](https://gitlab.com/rice-acosta-group/emtfpp/emtftoolssimulations) Simulations
    - Collection of common psets used to generate samples for training, and performance studies.
- [Repository](https://gitlab.com/rice-acosta-group/emtfpp/emtftoolsparticleguns) Particle Guns
    - Event generators for prompt and displaced signals.

An additional package exists for Analyzing Ntuples for Phase-2.

- [Repository](https://gitlab.com/rice-acosta-group/emtfpp/emtfpp-analysys) EMTF++ Analysis
    - The intention of this package is to provide a CLI with all the performance studies needed for EMTF.
    - Currently it only supports EMTF++ Analysis.

## Change Log

- 17/Mar/2023
    - Added initial docs.
