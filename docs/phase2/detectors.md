# Muon Detectors

![](assets/cms_muon_detectors.png)

- New forward muon detectors improve redundancy, efficiency and timing (only iRPC)
    - GEM detectors: GE1/1, GE2/1
    - iRPC detectors: RE3/1, RE4/1
    - ME0 detector

- New detectors also provide additional inputs and improved angular information (bend angles) for better correlation
with TT tracks and better standalone muon pT measurement.
- This requires increasing the bandwidth to the L1 muon electronics and adding more algorithm logic capability.

## CSC

1. The CSC trigger primitive is called a Local Charge Track (LCT).
2. The anode (cathode) digis are used to build the ALCT (CLCT).
3. The ALCT and CLCT are correlated to build the LCT.
4. The LCTs are built by the Trigger MotherBoard (TMB) and then sent to the Muon Port Card (MPC).
The data are concentrated by MPC and then sent to EMTF.

### Geometry

- ME1 has 3 rings, and the chambers are $`10^\circ`$ wide.
- ME2,3,4 have 2 rings, the ring 1 chambers are $`20^\circ`$ wide; and the ring 2 chambers are $`10^\circ`$ wide.
- ME1/1 is located at a different z position ($`\sim 6~m`$) compared to ME1/2 & ME1/3 ($`\sim 7~m`$).
The magnetic field is very different for ME1/1 because it is inside the solenoid ($`B_z=3.8~T`$).
The ME1/1 wires are tilted by $`29^\circ`$ to compensate for the drift of the electrons in the strong magnetic field.
- ME1/1 is further split into ME1/1a and ME1/1b at $`\eta \sim 2.05`$.
- Depending on the location of a chamber (even or odd chamber number, ME1,2 or ME3,4 positive or negative endcap),
a chamber can be closer or further from the interaction point. A F/R (front or rear) bit is assigned for each chamber
to take this into account.

### Strip Count
- ME1/1a has 48 strips
- ME1/1b has 64 strips.
- ME1/2 has 80 strips
- ME1/3 has 64 strips,
- ME2,3,4/n have 80 strips.

### Neighbors

![](assets/neighbor_sharing.png)

Neighboring CSC chambers overlap each other by 5 strip widths to avoid gaps between chambers, except in ME1/3.
ME1/3 is only approx 80% efficient, and the chamber size is $`\sim 8^\circ`$.

- ME1: chambers with CSCID=3,6,9, subsector=2 are shared between sector N and sector N+1.
- ME2,3,4: chambers with CSCID=3,9 are shared between sector N and sector N+1.

The strips and wires are independent of each other, built by cathode LCT processors and anode LCT processors separately.
If there are two hits in a given chamber, we will get 2 strip numbers and 2 wire numbers.
We do not know which strip number and which wire number is the right pair.
Thus, we consider all the possibilities. As there are at most 2 anode LCTs and at most 2 cathode LCTs in a given chamber,
there are at most 4 combinations to consider.

### Implementation
- $`\phi`$ of each LCT: 13-bit values (converted from 4-bit CSCID and 8-bit half-strip)
- $`\theta`$ of each LCT: 7-bit values (converted from 7-bit wiregroup)
- pattern ID for each LCT: 4-bit values
- F/R bit for each LCT: 1-bit values

In addition, we want to add a new bend variable, if it is possible to do in the TMB.
TAMU and UCLA have shown that doing a least squares fit to the CLCT comparator digis can improve
the $`\phi`$ position of each LCT (see Andrew Peck's talk).
But I find it more useful to extract the local bending angle from the fit,
where the bend is defined as the $`\Delta \phi`$ between the hit $`\phi`$'s at the innermost
and outermost layers of the CSC detector.

Basically, consider there are up to 6 hits in the 6 layers of the CSC detector.
One can do a simple linear fit with x = layer number and y = hit $`\phi`$.
Extract the slope of the fit and multiply it by 6 to obtain the new bend variable.
The result is a floating-point value. I multiply it by 4 (arbitrarily) before quantization.
As the CSC comparator digis are in half-strip unit, the bend is in 1/8-strip unit.
It should fit into 7 bits (-64 to 63).
But, it is only useful for ME1/1 and ME1/2 (see slides 7-9 of Jia Fu's talk).

## RPC

In Phase-1, EMTF use the RPC hits to improve efficiency and redundancy.
Specifically, these RPC chambers are received: RE1/2, RE2/2, RE3/2, RE3/3, RE4/2, and RE4/3.
To distinguish from the Phase-2 iRPC detectors, I will also refer to the Phase-1 RPC as "old RPC".

Old RPC hits are pre-processed by CPPF (which stands for Concentration, PreProcessing and Fanout),
which clusterizes the RPC digis and performs coordinate conversion,
before sending up to 2 clusters per RPC chamber to the EMTF.
The max cluster width is 3 strips. The integer $`\phi`$ and $`\theta`$ units used
by the CPPF coordinate conversion are 4x coarser compared to the units used for CSC,
because of the worse detector resolutions.

### Geometry

- 10°-wide chambers, 32 strips per chamber, 3 η partitions per chamber (or "rolls").

To ensure coverage near the sector boundaries, each sector procesor also receives one 10° chamber from the neighbor sector.

Note: The 'sector' according to RPCDetId starts at -5°. In EMTF, the RPC sector is re-defined according
to the CSC sector definition. Because RPC is a single-layer detector, it is more prone to noise.

The CPPF information used in EMTF are:

$`\phi`$ of each hit: 11-bit values
$`\theta`$ of each hit: 5-bit values

### Jia Fu Notes
> No changes for EMTF++ .
> But, on the hardware side, I believe CPPF will be replaced by newer electronics during the Phase-2 upgrade.

## iRPC

iRPC are improved RPC detectors that will be available for Phase-2.
The iRPC chambers are RE3/1 and RE4/1. In general, iRPC is very similar to RPC,
but with better spatial resolutions and timing resolution ($`\sim 1.5`$ ns).
See Phase-2 Muon TDR for more info.

### Geometry

- $`20^\circ`$-wide chambers, 192 strips per chamber, 5 $`\eta`$ partitions per chamber (or "rolls").

Since the iRPC chamber is 2x larger than the old RPC chamber, and has 6x more strips per chamber,
the $`\phi`$ resolution is 3x better. Like RPC, iRPC is a single-layer detector, and it is more prone to noise.

### Jia Fu Notes
> - I was told that iRPC will have 2D readout, which will provide better $`\eta`$ or $`\theta`$ resolution.
> However, the 2D readout was not simulated in the CMSSW release that I'm using.
> Also, I'm not sure about the exact cluster width cut.
> I apply a cut of 9 strips (arbitrarily).
> I also keep only up to 2 clusters per chamber (arbitrarily).
> These need to be checked with the experts.
> - Due to a bug that I only discovered just now, I was using the 4x coarser CPPF coordinate conversion for iRPC hits.
> I should be using the EMTF coordinate conversion since iRPC has better resolutions.
> This will be fixed soon.
> - In the TDR, iRPC is stated to have 96 strips per chamber.
> But in the CMSSW simulation, I found 192 strips per chamber.
> This needs to be investigated.
> - At the moment, I don't make use of the 1.5ns timing info, because I'm worried
> that the simulation performance might not be very realistic.
> It can always be added in the future when we become more confident.
> In fact, it's supposed to be very important to help triggering for HSCPs (heavy stable charged particles).

## GEM

GEM (Gas Electron Multiplier) is a new type of detector to be installed at GE1/1 and GE2/1. In particular, GE1/1 can provide a bend angle between GE1/1 and ME1/1, which is claimed to significantly improve the pT measurement. GE1/1 is planned to be installed during LS2, and be ready for physics in Run 3; GE2/1 will be installed during LS3 together with the rest of new Phase-2 detectors. See GEM TDR for more info about GE1/1, and Phase-2 Muon TDR for more info about GE2/1.

### Geometry

- GE1/1 is 10° wide with 192 pads; GE2/1 is 20° wide with 384 pads. A pad is 2 strips ganged together, and it is the unit that will be transmitted to EMTF.
- GE1/1 and GE2/1 have the same ϕ resolution.
- GE1/1 and GE2/1 have 8 η partitions per chamber.
- GE1/1 and GE2/1 are 2-layer detectors. A coincidence is required in the two layers (with a window of ±3 pads for GE1/1, ±2 pads for GE2/1)

### Jia Fu Notes
> According to Sven Dildick (TAMU), the GEM pads will be clustered before they are sent to EMTF.
> The max cluster width is 8 pads, and there are at most 8 clusters per layer per chamber.
> According to his instructions, EMTF is supposed to declusterize the clusters to retrieve the original pads
> and use them (up to 64 pads) in the trigger, so as not to "lose" resolution.
>
> I'm not sure what is the merit of these instructions,
> but I have followed the instructions in my study.
> An alternative is to simply use the clusters.
>
> Furthermore, TAMU has developed an algorithm that combine GEM & CSC hits
> to form a new trigger primitives called ILT (integrated LCT).
> This is done to:
>     1. reduce the number-of-layers requirement for the CSC LCT from 4 to 3, which improves efficiency;
>     2. improve background rejection with the increased total number of layers during the trigger primitive reconstruction.
>     The ILT algorithm is currently not used in my study, but it can be added in the future.

## ME0

ME0 is a 6-layer GEM detector that will be mounted at the "nose" of the first endcap disk.
A ME0 segment will be built by dedicated electronics, and EMTF will receive the $`\phi`$, $`\theta`$ (or $`\eta`$), the bend and the quality.
The bend is defined as the $`\Delta \phi`$ between the hit $`\phi`$'s at the innermost and the outermost ME0 detector layers.
Both the $`\phi`$ and bend are extracted from a simple linear fit to the ME0 digis in the 6 layers.
See Phase-2 Muon TDR for more info.

### Geometry

![](assets/me0_alignment.png)

- ME0 chambers are $`20^\circ`$ wide, 384 strips (192 pads) per chamber, 8 $`\eta`$-partitions per chamber.
- The ME0 detectors begin at $`-10^\circ`$, while the CSC detectors begin at $`-5^\circ`$.

### Phi Measurements

- In the positive endcap the half-strip number increases counter-clockwise.
- In the negative endcap the half-strip number increases clockwise.

### Jia Fu Notes
> - I'm not sure whether we will receive ME0 pad numbers or the converted ϕ and η coordinates. I assume the latter in my study.
> - ME0 extends from $`\eta`$ = 2.0 to 2.8. Although this allows EMTF to trigger beyond η of 2.4, this is not done in my study, because it adds extra rates, and makes it more confusing when doing rate comparisons.


### Implementation

- In order to match the ME0 Chambers to the CSC Chambers, the $`20^\circ`$ CSC Chamber were split
into two virtual $`10^\circ`$ CSC Chambers.

## Summary

The following table shows the spatial resolutions for all the muon detectors. (Maybe adding DT too?)

![](assets/chamber_types_and_resolutions.png)

The relative $`\phi`$ resolution is relative to CSC $`10^\circ`$ chambers (non-ME1/1).
For ME0, I assume the $`\phi`$ coordinate extracted from the fit has a resolution of
$`20^\circ / 384 \times 1/\sqrt{6} `$.

In the Phase-2 scenario, each sector processor may receive up to 95 links.
For the numbers of input links coming from each detector type, see slide 15 in Darin's talk.
