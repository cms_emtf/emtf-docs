# Phase2 EMTF

## General Information

EMTF is a regional track finder system without time multiplexing,
which covers the $`\eta`$ region from 1.24 to 2.4.
The EMTF system consists of 12 sector processors - 6 for each endcap,
1 for each $`60^\circ`$ trigger sector (at $`15^\circ`$, $`75^\circ`$, ...).
Each sector works independently. To ensure coverage near the sector boundaries,
each sector N also shares certain chambers with the neighbor sector N+1.
Thus, although it is nominally $`60^\circ`$, each sector actually covers $`70^\circ`$
in the rings with $`10^\circ`$-wide chambers, or $`80^\circ`$ in the rings with $`20^\circ`$-wide chambers.

![](assets/trigger_sectors.png)

In Phase-1, EMTF receives two types of trigger primitives: ⬛ **CSC** and ⬛ **RPC** hits.
In Phase-2, EMTF++ will receive additional ⬛ **GEM**, ⬛ **iRPC** and ⬛ **ME0** trigger primitives.
But, GE1/1 in particular will be installed and running after LS2.
Muon momenta are measured from the $`\phi`$ and $`\eta`$ coordinates of these trigger primitives
as the muons traverse the endcap disks in the non-uniform magnetic field.

## Algorithm Overview

The EMTF algorithm starts by receiving the hits, or trigger primitives,
and ends by producing muon tracks with reconstructed information:
(most importantly) $`p_T`$ , $`\eta`$, $`\phi`$, track quality, and its associated hits. 
EMTF++ is an extension of the EMTF algorithm, and it follows the same design, shown below.

![](assets/algorithm_blocks.svg)

