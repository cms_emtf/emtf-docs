# Phase 2 Sector Processor Firmware

## Overview

The EMTF Sector Processor module handles the whole EMTF algorithm. It takes in trigger primitives, converts them into EMTF segments, builds the tracks, and assigns the parameters using Neural Networks. This page covers the firmware implementation details.

![](assets/algorithm_blocks.svg)

## Module Overview and SLR Layout

## Primitive Conversion

## Track Finding

### Hitmap Building

### Pattern Matching

### Sorting the Best Tracks

## Track Building

## Neural Networks


